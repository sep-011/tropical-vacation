﻿function signup() {

    // Create our XMLHttpRequest object
    var hr = new XMLHttpRequest();
    // Create some variables we need to send to our PHP file

    var url = "http://sep.esy.es/sep/nsignup.php";
    var email = document.getElementById("email").value;

    var atindex = email.indexOf("@");
    var emailA = email.substring(0, atindex);
    var emailB = email.substring(atindex + 1, email.length);

    var password = document.getElementById("password").value;
    var confirm_password = document.getElementById("confirm_password").value;

    var fname = document.getElementById("fname").value;
    var lname = document.getElementById("lname").value;

    if (lname.length == 0 || fname.length == 0 || password.length == 0 || confirm_password.length == 0) {


        document.getElementById("error").innerHTML = "Please fill all the fields!";
    }
    else if (!isNaN(fname) || !isNaN(lname)) {
        document.getElementById("error").innerHTML = "Cannot have digits!";
    }
    else if (!emailvalidation()) {

        document.getElementById("error").innerHTML = "Provide a correct email!";

    }
    else if (!passwordmatching(password, document.getElementById("confirm_password").value)) {
        // alert("password doesnot matching !")
        document.getElementById("error").innerHTML = "password doesnot matching !";
    }

    else {

        var vars = "fname=" + fname + "&lname=" + lname + "&emailA=" + emailA + "&emailB=" + emailB + "&password=" + password;
        hr.open("POST", url, true);
        // Set content type header information for sending url encoded variables in the request
        hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // Access the onreadystatechange event for the XMLHttpRequest object
        hr.onreadystatechange = function () {
            if (hr.readyState == 4 && hr.status == 200) {
                var return_data = hr.responseText;
                var str = return_data;

                var pos = str.indexOf("%");
                var status = str.substring(pos + 1, str.length);
                if (status.trim() == "created") {
                    localStorage.useremail = email;
                    window.location = "logins.html";

                }


                else if (status.trim() == "exist") {
                    // alert("login failed!");
                    document.getElementById("error").innerHTML = "Email is exist";

                }
                else
                    document.getElementById("error").innerHTML = "Error !!";


            }
        }
        // Send the data to PHP now... and wait for response to update the status div
        hr.send(vars); // Actually execute the request



    }
}

function passwordstrongest() {
    var password = document.getElementById("password").value;
    if (password.length < 5) {
        // alert("low password strengrh");
        document.getElementById("error").innerHTML = "low password strengrh!";
    }
    else
        // alert("good password strengrh");
        document.getElementById("error").innerHTML = "good password strengrh";
}
function passwordmatching(apassword, aconfrim_password) {

    var password = apassword;

    var confirm_password = aconfrim_password;
    if (password != confirm_password) {

        return false;
    }
    else {
        return true;
    }
}
function password() {

    var password = document.getElementById("password").value;

    var confirm_password = document.getElementById("confirm_password").value;
    if (password != confirm_password) {
        //alert("does not matching");
        document.getElementById("error").innerHTML = "Password does not matching";

        return false;
    }
    else {
        return true;
    }
}
function emailvalidation() {
    // alert("You have entered an invalid email address!");
    var input_email = document.getElementById("email").value;

    // alert(input_email);
    //  document.getElementById("email)").value
    //var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
    if (!pattern.test(input_email)) {
        document.getElementById("error").innerHTML = "Not a email address!";
        return false;
    }
    else {
        document.getElementById("error").innerHTML = "";
        return true;
    }



}
