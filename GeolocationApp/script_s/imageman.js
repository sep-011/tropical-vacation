﻿var imgUri;

var imageselected = false;


function getImage() {
  
    // Retrieve image file location from specified source
    navigator.camera.getPicture(onPhotoURISuccess, onFail, {
        quality: 50,
        destinationType: navigator.camera.DestinationType.FILE_URI,
        sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth: 250,
        targetHeight: 250,
    });
}

// Called if something bad happens.
//
function onFail(message) {
    alert('Failed because: ' + message);
}

function onPhotoURISuccess(imageURI) {


    imgUri = imageURI;

    document.getElementById('imgprofilepicholder').src = imgUri;
    $('#uploadbutton').show();
   

}
function uploadPhoto() {
   
        $.mobile.loading("show", {
        textVisible: true
         });
        var options = new FileUploadOptions();
        options.fileKey = "file";

        options.fileName = imgUri.substr(imgUri.lastIndexOf('/') + 1);

        options.mimeType = "image/jpeg";

        var params = new Object();

       
        params.newimage =true;
        params.uid =localStorage.uid;

        options.params = params;
        options.chunkedMode = false;

        var ft = new FileTransfer();

        ft.upload(imgUri, "http://sep.esy.es/sep/iteration4storm/changeprofilepic.php", win, fail, options);
        $.mobile.loading("hide");

}

function win(r) {
    document.getElementById('status').style.color = "Green";
    document.getElementById('status').innerHTML = r.response;

}

function fail(error) {
    alert("An error has occurred: Code = " + error.code);
}

//profil pic
function loadtheprofilepic()
{
    $.mobile.loading("show", {
        textVisible: true
    });
    $.post("http://sep.esy.es/sep/iteration4storm/changeprofilepic.php",
                {
                    uid: localStorage.uid,
                    changepic:true
                },
               function (data, status) {
                   $.mobile.loading("hide");
                   document.getElementById("divprofilepic").innerHTML =data;
               });
    /*if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {  // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("divprofilepic").innerHTML = xmlhttp.responseText;
            $.mobile.loading("hide");
        }
    }
    xmlhttp.open("GET", "http://localhost/sep/iteration4storm/changeprofilepic.php?uid=" + localStorage.uid + "&changepic=true", true);
    $.mobile.loading("show", {
        textVisible: true
    });
    xmlhttp.send();*/
}