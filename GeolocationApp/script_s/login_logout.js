﻿function logout() {
    localStorage.lstatus = 00;
    localStorage.uid = 0;
    document.getElementById('blogout').style.visibility = 'hidden';
    document.getElementById('blogin').style.visibility = 'visible';
    window.location = "index.html";

}
function checklogstatus() {

    if (localStorage.lstatus == undefined) {
        localStorage.lstatus = 00;
        localStorage.uid = 0;
    }

    if (localStorage.lstatus != 0011) {
        document.getElementById('blogout').style.visibility = 'hidden';
        document.getElementById('blogin').style.visibility = 'visible';
    }
    else {
        document.getElementById('blogout').style.visibility = 'visible';
        document.getElementById('blogin').style.visibility = 'hidden';
        document.getElementById('bsignup').style.display = 'none';

    }
}

function changtologin() {

        //If user not logged in redirect to the login page
        if (localStorage.uid == 0 || localStorage.uid == undefined) {
            window.location = "logins.html";
        }
}