﻿

function MobileLoad_hide()
{
    $.mobile.loading("hide");
}

function MobileLoad_show()
{
    $.mobile.loading("show", {
        textVisible: true
    });
}

function setuserid() {
    if (localStorage.uid == 0) {

        window.location = "logins.html";
    }
    else {
        document.getElementById('id1').value = localStorage.uid;
    }
    
}


//Seach for Locations details
function showResult(str,type) {

    if (str.length == 0) {
        document.getElementById("livesearch").innerHTML = "";
        document.getElementById("livesearch").style.border = "0px";
        return;
    }

    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("livesearch").innerHTML = xmlhttp.responseText;
            document.getElementById("livesearch").style.border = "1px solid #A5ACB2";

            MobileLoad_hide();
        }
    }
    xmlhttp.open("GET", "http://sep.esy.es/sep/plan/App_requests/viewrequests.php?q=" + str +"&type="+type, true);
    
    xmlhttp.send();

    
    MobileLoad_show();

}


/*Function Calling happening from the php page
/*Showing the selected location    
*/
function ShowSelect(locid) {


    document.getElementById("livesearch").innerHTML = "";

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/viewrequests.php";
    

    var vars = "locid=" + locid;

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("originview").innerHTML = return_data;
            
            MobileLoad_hide();
        }
    }


    hr.send(vars);

    document.getElementById('originid').value = locid;
    
    MobileLoad_show();
}

/*
/*Search for locations details ( desired places )
*/
function Showdplaces(locid) {


    document.getElementById("livesearch").innerHTML = "";

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/viewrequests.php";


    var vars = "did=" + locid;

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("dplaces").innerHTML = return_data;

            MobileLoad_hide();
        }
    }


    hr.send(vars);

    MobileLoad_show();

}

//Add Travel Plan using user's enterd data
function AddTravelPlan() {

   
    var uid = document.getElementById('id1').value;
    
    if (document.getElementById("pname").value != "")
        var pname = document.getElementById('pname').value;
    else
        alert("please enter plan name");

    if (!isNaN(document.getElementById("adult").value))
        var adtr = document.getElementById('adult').value;
    else
        alert("Please enter a number");

    if (!isNaN(document.getElementById("child").value))
        var chtr = document.getElementById('child').value;
    else
        alert("Please enter a number");

    var bcls = document.getElementById('budget').value;
    var tm = document.getElementById('tmethod').value;
    var locid = document.getElementById('originid').value;


    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("result3").innerHTML = xmlhttp.responseText;
            if (xmlhttp.responseText == "Plan Added Successfully")
            {
                document.getElementById("prc").style.display = 'block';
            }

            MobileLoad_hide();
            
        }
    }
    xmlhttp.open("GET", "http://sep.esy.es/sep/plan/App_requests/transrequests.php?id="
        + uid + "&pname=" + pname + "&adtr=" + adtr + "&chtr=" + chtr + "&bcls=" + bcls + "&tm=" + tm + "&locid=" + locid, true);
    

   

    xmlhttp.send();

    MobileLoad_show();
    
}

//To save desired places
function SaveDplaces(locid) {

    
    var uid = document.getElementById('id1').value;

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/transrequests.php";


    var vars = "locid=" + locid + "&uid=" + uid;
    

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("added").innerHTML = return_data;
            
            MobileLoad_hide();
        }
    }


    hr.send(vars);

    ViewAddedDplaces();

    MobileLoad_show();
}

//view added desired places (so that user can remove )
function ViewAddedDplaces() {

    var uid = document.getElementById('id1').value;

    document.getElementById("livesearch").innerHTML = "";
    document.getElementById("added").innerHTML = "";
    document.getElementById("dplaces").innerHTML = "";

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/viewrequests.php";


    var vars = "ad=" + uid;

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("dpl").innerHTML = return_data;

            MobileLoad_hide();
        }
    }


    hr.send(vars);

    MobileLoad_show();

}

//remove un wanted places 
function RemoveDplace(locid) {

    document.getElementById("livesearch").innerHTML = "";
    document.getElementById("added").innerHTML = "";
    document.getElementById("dpl").innerHTML = "";


    var uid = document.getElementById('id1').value;

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/transrequests.php";


    var vars = "lid=" + locid + "&id=" + uid;


    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            
            MobileLoad_hide();
        }
    }


    hr.send(vars);

    ViewAddedDplaces();

    MobileLoad_show();

}


/*
/*Get Latitude and longitude of the locations
/* This latitude and longitude uses for distance matrix of the google api
*/
function Getplaceslatlng() {

    
    var value = document.getElementById('id1').value;

    var value2 = document.getElementById('pid').value;

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/viewrequests.php";


    var vars = "uid=" + value + "&pid=" + value2;

    

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("hdn").innerHTML = return_data;

            setorigin();

            setdestinations();

            MobileLoad_hide();
        }
    }


    hr.send(vars);

    MobileLoad_show();

}

/*
/* Once user select locations to his plan
/* Distance metric calculation should be begin with
/* setting previously added location as origin
/* Function use to get Latitude longitudes
*/
function GetplaceslatlngMod() {


    var value = document.getElementById('id1').value;

    var value2 = document.getElementById('pid').value;

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/viewrequests.php";


    var vars = "userid=" + value + "&planid=" + value2;



    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("hdn").innerHTML = return_data;

            setorigin();

            setdestinations();

            MobileLoad_hide();
        }
    }


    hr.send(vars);


    MobileLoad_show();
}


/*
/* To get Last added plan id 
/* when user in the process of creating a plan
/* He use last added plan id as current plan ids
*/
function Getgrpid() {


    var value = document.getElementById('id1').value;

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/viewrequests.php";


    var vars = "id=" + value;

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("piddiv").innerHTML = return_data;
            Getplaceslatlng();

            MobileLoad_hide();
        }
    }


    hr.send(vars);

    MobileLoad_show();

}


var origin1; // Use to store the origin latitude longitude as googlemap latlng value
var destArray = []; //Store desired places latitude longitude as googlemap latlng values

var locationid = []; // To backup the location id

var i = 0;

// Setting the origin latlng value
function setorigin() {

   
    var latln = document.getElementById('origin').innerHTML;

    var res = latln.split(",");

    origin1 = new google.maps.LatLng(res[0], res[1]);
    
}

// setting the desired places latlng value
// Going through a loop for all the retrived places
// max value set from the php page
function setdestinations() {

    
    //get destination count to use in loop
    var max = document.getElementById("count").innerHTML;

    for (i = 0; i < max; i++) {

        var latln = document.getElementById(i).innerHTML;
        var res = latln.split(",");
        destArray[i] = new google.maps.LatLng(res[0], res[1]);
        locationid[i] = res[2];
    }
}





/* Distance Calculation
    GetDistanceMatrix use callback method as a parameter
*/
function calculateDistances() {
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
      {
          origins: [origin1],
          destinations: destArray,
          travelMode: google.maps.TravelMode.DRIVING,
          unitSystem: google.maps.UnitSystem.METRIC,
          avoidHighways: false,
          avoidTolls: false
      }, callback);
}


/*
    Using origin and destinations calculating the distance matrix
    distance matrix --> distance, time 
    formatted out put is displaying
*/
function callback(response, status) {
    if (status != google.maps.DistanceMatrixStatus.OK) {
        alert('You desired places saved completed');
        Savedestination();
        document.getElementById("prc").style.display = 'block';

    } else {
        var origins = response.originAddresses;
        
        var destinations = response.destinationAddresses;
        var outputDiv = document.getElementById('outputDiv');
        outputDiv.innerHTML = '';
        

        for (var i = 0; i < origins.length; i++) {
            var results = response.rows[i].elements;
            
            for (var j = 0; j < results.length; j++) {
               
                var convert = results[j].distance.value / 1000;//convert to km

                outputDiv.innerHTML += origins[i] + ' to ' + destinations[j]
                    + ': ' + results[j].distance.text + ' in '
                    + results[j].duration.text + '<br>' +
                    '<p><input style="display:none;" id="' + "s" + j + '" value="' + convert + "&" + results[j].duration.text + '" /></p>' +
                    '<button onclick="javascript:SaveToplan(' + j + ');">Save To Plan</button><br>';

                

            }



        }
    }
}

/*
    Method calling from the callback(response, status) method created button "Save to Plan" 
*/
function SaveToplan(value) {
    

    var details = document.getElementById("s" + value).value;

    var data = details.split("&");

    var dist = data[0];
    var duration = data[1];


    var locdetails = document.getElementById(value).innerHTML;
    var res = locdetails.split(",");
    locationid = res[2];

    SaveMultiple(locationid, dist, duration);

}

// To save selected desired places traveling details to the travel plan
function SaveMultiple(locid,dist,duration) {


    var pid = document.getElementById('pid').value;
    var uid = document.getElementById('id1').value;

    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("outputDiv2").innerHTML = xmlhttp.responseText;
            
            document.getElementById("outputDiv").innerHTML = "";

            origin1 = null;
            destArray.length = 0;
            locationid.length = 0;

            GetplaceslatlngMod();

            MobileLoad_hide();
        }
    }
    xmlhttp.open("GET", "http://sep.esy.es/sep/plan/App_requests/transrequests.php?pid=" + pid + "&locid=" + locid + "&dist="+ dist + "&du="+duration+ "&uid="+uid, true);

    xmlhttp.send();

    MobileLoad_show();
}



// To get Places that saved to travel plan
function GetPlanPlaces() {

    
    var value2 = document.getElementById('pid').value;

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/viewrequests.php";


    var vars = "pid=" + value2 ;



    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("multi").innerHTML = return_data;

            MobileLoad_hide();
        }
    }


    hr.send(vars);

    MobileLoad_show();
}


// Get Hotels details relavent to the selected desired place
function GetHotels(locid) {

    var value2 = document.getElementById('pid').value;

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/viewrequests.php";


    var vars = "location=" + locid + "&plid=" + value2;



    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("hotel").innerHTML = return_data;

            MobileLoad_hide();
        }
    }


    hr.send(vars);

    MobileLoad_show();
}


// To Save hotel Details
function SaveHotels(locid, hotelid) {

    

    var pid = document.getElementById('pid').value;


    if (!isNaN(document.getElementById("rooms").value))
        var rooms = document.getElementById("rooms").value;
    else
        alert("please enter number");

    if (!isNaN(document.getElementById("days").value))
        var days = document.getElementById("days").value;
    else
        alert("please enter number ");
    

    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

            document.getElementById("out").innerHTML = xmlhttp.responseText;
            document.getElementById("hotel").innerHTML = "";

            MobileLoad_hide();
        }
    }
    xmlhttp.open("GET", "http://sep.esy.es/sep/plan/App_requests/transrequests.php?loid=" + locid + "&hid=" + hotelid + "&plid=" + pid +"&rooms=" + rooms + "&day="+days, true);

    xmlhttp.send();

    MobileLoad_show();
}


// To select previously created travel plan
// Retrive user's all travel plans
function selectPlan() {


    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/viewrequests.php";

    var value = document.getElementById('id1').value;

    var pid = document.getElementById('pid').value;

    var vars = "user_id=" + value + "&plan_id=" + pid;

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result").innerHTML = return_data;

            MobileLoad_hide();


        }
    }


    hr.send(vars);

    MobileLoad_show();

}

// Set selected travel plan as current travel plan
function setplanlocal(planid) {

    localStorage.setItem("planid", planid);
    Get_Selectedpid();
    //selectPlan();
    document.getElementById("result").innerHTML = "";
    document.getElementById("multi").innerHTML = "";
    document.getElementById("hotel").innerHTML = "";
}


// To Show last resul of the created travel plan
function ShowResults()
{

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/viewrequests.php";

    var value = document.getElementById('id1').value;

    var pid = document.getElementById('pid').value;

    var vars = "usid=" + value + "&pnid=" + pid;

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result").innerHTML = return_data;

            MobileLoad_hide();


        }
    }


    hr.send(vars);

    MobileLoad_show();

}

// After arranging the travel places last place is the destination
// To save the detination to Travel plan
function Savedestination() {

    
    var value = document.getElementById('id1').value;

    var value2 = document.getElementById('pid').value;

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/transrequests.php";


    var vars = "userid=" + value + "&planid=" + value2;



    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            
            MobileLoad_hide();

        }
    }


    hr.send(vars);

    MobileLoad_show();

}

// To get The selected plan id
function Get_Selectedpid() {


    var value = localStorage.getItem("planid");

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/plan/App_requests/viewrequests.php";


    var vars = "selpid=" + value;

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("piddiv").innerHTML = return_data;
            Getplaceslatlng();

            MobileLoad_hide();
        }
    }


    hr.send(vars);

    MobileLoad_show();

}