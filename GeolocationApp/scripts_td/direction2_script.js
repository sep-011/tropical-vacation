﻿
var map;

var markerArray = [];

var latlntemp;
var marker = null;


var directionsDisplay;
var directionsService = new google.maps.DirectionsService();


var nlatlng;


//initializing geolocation
function initGeolocation() {
    if (navigator && navigator.geolocation) {
        var watchId = navigator.geolocation.watchPosition(successCallback,
                                                          errorCallback,
                                                          { enableHighAccuracy: true, timeout: 60000, maximumAge: 0, enableHighAccuracy: true, frequency: 3000 });

    } else {
        console.log('Geolocation is not supported');
    }

    getlocations2();
    //initialize();
}

//handling errors
function errorCallback(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.");
            break;
    }

}

// get lat & long then display map
function successCallback(position) {
    var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);


    directionsDisplay = new google.maps.DirectionsRenderer();



    // Create a map and center it on currentlocation.
    var currentlocation = new google.maps.LatLng(myLatlng.lat(), myLatlng.lng());
    if (map == undefined) {
        var mapOptions = {
            zoom: 13,
            center: currentlocation

        }

        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById('directions-panel'));

        var control = document.getElementById('control');
        control.style.display = 'block';
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(control);


        //document.getElementById('no1').value = myLatlng.lat().toString() + "," + myLatlng.lng().toString();
        nlatlng = myLatlng.lat().toString() + "," + myLatlng.lng().toString();

        getmarker(myLatlng);

    }
    else {
        map.panTo(myLatlng);
        getmarker(myLatlng);
        document.getElementById('no1').value = myLatlng.lat().toString() + "," + myLatlng.lng().toString();
        nlatlng = myLatlng.lat().toString() + "," + myLatlng.lng().toString();
        
    }

}

//marker creation
function getmarker(mylatlng) {
    if (marker == null) {
        marker = new google.maps.Marker({ map: map, position: mylatlng, animation: google.maps.Animation.DROP });
    }
    else {
        marker.setPosition(mylatlng);


    }
    latlntemp = mylatlng;
}




function calcRoute() {

    if (document.getElementById('start').selectedIndex == 0) {
        var start = nlatlng;
    }
    else {
        var start = document.getElementById('start').value;
    }
    var end = document.getElementById('end').value;
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });
}

//google.maps.event.addDomListener(window, 'load', initialize);


function getlocations2() {
    
    var hr = new XMLHttpRequest();
    
    var url = "http://sep.esy.es/sep/travel/directiondatalong.php";
    var query = 1;
    var vars = "all=" + query;
    hr.open("POST", url, true);
    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result").innerHTML = return_data;
        }
    }
    
    hr.send(vars); 
    document.getElementById("result").innerHTML = "processing...";

}