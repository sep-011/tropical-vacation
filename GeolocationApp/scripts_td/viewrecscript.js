﻿


//view the recommended places from database
function getrecommendedplaces() {

    
    var hr = new XMLHttpRequest();

   
    var url = "http://sep.esy.es/sep/travel/getrecommendplaces.php";

    var query = document.getElementById("id1").value;

    var vars = "fav=" + query;

    hr.open("POST", url, true);

    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result").innerHTML = return_data;

            $.mobile.loading("hide");

        }
    }

    
    hr.send(vars); 

    //document.getElementById("result").innerHTML = "processing...";

    $.mobile.loading("show", {
        textVisible: true
    });

    
    $("body").animate({ scrollTop: $('#status').offset().top - 20 }, "slow");

}//end of the getrecommendedplaces function





function getRecplacesdetails(placeid) {
    
    var hr = new XMLHttpRequest();
    
    var url = "http://sep.esy.es/sep/travel/getrecplacesdetails.php";
    var query = placeid;
    var vars = "q=" + query;
    hr.open("POST", url, true);
    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("status").innerHTML = return_data;

            $.mobile.loading("hide");

        }
    }
    
    hr.send(vars); 
    //document.getElementById("status").innerHTML = "processing...";

    $.mobile.loading("show", {
        textVisible: true
    });

    $("body").animate({ scrollTop: $('#status').offset().top - 20 }, "slow");

}//end of the getRecplacesdetails function


function getsuggestedplaces() {

   
    var hr = new XMLHttpRequest();

    
    var url = "http://sep.esy.es/sep/travel/suggestedplaces.php";

    var query = document.getElementById("id1").value;

    var vars = "sug=" + query;

    hr.open("POST", url, true);

    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result").innerHTML = return_data;

            $.mobile.loading("hide");

        }
    }

    
    hr.send(vars); 

    //document.getElementById("result").innerHTML = "processing...";
    $.mobile.loading("show", {
        textVisible: true
    });

    $("body").animate({ scrollTop: $('#status').offset().top - 20 }, "slow");

}//end of the getsuggestedplaces function





function Addtravelplan(id, uid) {
    
    var hr = new XMLHttpRequest();
    
    var url = "http://sep.esy.es/sep/travel/addtravelplan.php";

    var query = id;
    var usid = uid;
    var vars = "pid=" + query + "&ud=" + usid;
    hr.open("POST", url, true);
    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("status").innerHTML = return_data;

            $.mobile.loading("hide");

        }
    }
    
    hr.send(vars); 

    //document.getElementById("status").innerHTML = "processing...";

    $.mobile.loading("show", {
        textVisible: true
    });

    $("body").animate({ scrollTop: $('#status').offset().top - 20 }, "slow");

    
    $("#status").empty();
    $("#result").empty();

    getsuggestedplaces();

}//end of Addtravelplan function


function removetravelplan(id, uid) {
    
    var hr = new XMLHttpRequest();
    
    var url = "http://sep.esy.es/sep/travel/removeplacefromtp.php";

    var query = id;
    var usid = uid;
    var vars = "pid=" + query + "&ud=" + usid;
    hr.open("POST", url, true);
    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("status").innerHTML = return_data;

            $.mobile.loading("hide");

        }
    }
    
    hr.send(vars); 

    //document.getElementById("status").innerHTML = "processing...";

    $.mobile.loading("show", {
        textVisible: true
    });

    $("body").animate({ scrollTop: $('#status').offset().top - 20 }, "slow");



    $("#status").empty();
    $("#result").empty();

    getsuggestedplaces();

}//end of Addtravelplan function



//Weather
function getweatherdetails() {

    
    var hr = new XMLHttpRequest();

    
    var url = "http://sep.esy.es/sep/travel/weatherdetails.php";

    var query = document.getElementById('start').value;

    var vars = "zipcode=" + query;

    hr.open("POST", url, true);

    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result").innerHTML = return_data;

            $.mobile.loading("hide");

        }
    }

    
    hr.send(vars); 

    //document.getElementById("result").innerHTML = "processing...";

    $.mobile.loading("show", {
        textVisible: true
    });


    $("body").animate({ scrollTop: $('#status').offset().top - 20 }, "slow");

}//end of the getweatherdetails function


function loadwethear() {

    setuserid();
    
    var hr = new XMLHttpRequest();

    
    var url = "http://sep.esy.es/sep/travel/getweatherid.php";

    var query = document.getElementById('id1').value;

    var vars = "uid=" + query;

    hr.open("POST", url, true);

    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("dropdown").innerHTML = return_data;

            $.mobile.loading("hide");


        }
    }

    
    hr.send(vars); 

    //document.getElementById("dropdown").innerHTML = "processing...";

    $.mobile.loading("show", {
        textVisible: true
    });

    

}//end of the loadwethear function

///////////////////


/////Distances

function getdistances() {

    //document.getElementById('id2').value = "6.9344" + "," + "79.8428";

    initGeolocation();

   
    var hr = new XMLHttpRequest();

   
    var url = "http://sep.esy.es/sep/travel/getdistances.php";

    var query = document.getElementById("id1").value;

    var latlg = document.getElementById("id2").value;

    var vars = "dis=" + query + "&ltln=" + latlg;

    hr.open("POST", url, true);

    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result").innerHTML = return_data;

            $.mobile.loading("hide");

        }
    }

    
    hr.send(vars); 

    //document.getElementById("result").innerHTML = "processing...";
    $.mobile.loading("show", {
        textVisible: true
    });


    $("body").animate({ scrollTop: $('#status').offset().top - 20 }, "slow");

}//end of the getdistances function





///////////////////////////



//stop redirect
$("#RecForm1").submit(function () {
    return false;
});


//clear div
$(document).ready(function () {
    $("#get1").click(function () {
        $("#status").empty();
    });
});
$(document).ready(function () {
    $("#get2").click(function () {
        $("#status").empty();
    });
});




//initializing geolocation
function initGeolocation() {
    if (navigator && navigator.geolocation) {
        var watchId = navigator.geolocation.watchPosition(successCallback,
                                                          errorCallback,
                                                          { enableHighAccuracy: true, maximumAge: 0, enableHighAccuracy: true, frequency: 3000 });

    } else {
        console.log('Geolocation is not supported');
    }

    setuserid();
}

//handling errors
function errorCallback(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.");
            break;
    }

}

// get lat & long then display map
function successCallback(position) {
    var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);



    document.getElementById('id2').value = myLatlng.lat().toString() + "," + myLatlng.lng().toString();




}

//clear div
$(document).ready(function () {
    $("#get1").click(function () {
        $("#status").empty();
    });
});

$(document).ready(function () {
    $("#get2").click(function () {
        $("#status").empty();
    });
});

$(document).ready(function () {
    $("#clearall").click(function () {
        $("#status").empty();
        $("#result").empty();

    });
});

//table hover
$(document).ready(function () {

    $('#myTable2 tr').click(function () {
        var href = $(this).find("a").attr("href");
        if (href) {
            window.location = href;
        }
    });

});


//swipe control
$(document).on("swipeleft swiperight", "#pageone", function (e) {
    console.log('swiped!!')
    //check if there is no open panel on the page 
    if ($.mobile.activePage.jqmData("panel") !== "open" && $(e.target).closest(".bxslider").length === 0) {
        if (e.type === "swiperight") {
            $("#myPanel").panel("open");
        }
        
    }
    else if ($.mobile.activePage.jqmData("panel") == "open" && $(e.target).closest(".bxslider").length === 0) {
        $("#myPanel").panel("close");
        
    }
});


function setuserid() {
    if (localStorage.uid == 0) {
        //alert('please log to comment');
        window.location = "logins.html";
    }
    else {
        document.getElementById('id1').value = localStorage.uid;
    }
    //document.getElementById('id1').value = localStorage.uid;
}