﻿
function MobileLoad_hide() {
    $.mobile.loading("hide");
}

function MobileLoad_show() {
    $.mobile.loading("show", {
        textVisible: true
    });
}



// To select group name
function showResult(str) {



    if (str.length == 0) {
        document.getElementById("livesearch").innerHTML = "";
        document.getElementById("livesearch").style.border = "0px";
        return;
    }

    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("livesearch").innerHTML = xmlhttp.responseText;
            document.getElementById("livesearch").style.border = "1px solid #A5ACB2";

            MobileLoad_hide();
        }
    }
    xmlhttp.open("GET", "http://sep.esy.es/sep/group/App_requests/viewrequests.php?q=" + str, true);
    
    xmlhttp.send();

    MobileLoad_show();
}




// To add new Group
function Addnewgrp() {

    if (document.getElementById("grpname").value != "")
        str = document.getElementById("grpname").value;
    else
        alert("please enter a group name");


    uid = document.getElementById("id1").value;

    if (document.getElementById("ocode").value != "")
        var ocode = document.getElementById("ocode").value;
    else
        alert("please enter a secret code");

    

    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("result").innerHTML = xmlhttp.responseText;

            MobileLoad_hide();
        }
    }
    xmlhttp.open("GET", "http://sep.esy.es/sep/group/App_requests/transrequests.php?q=" + str + "&uid=" + uid + "&code=" + ocode, true);
    
    xmlhttp.send();

    document.getElementById("grpname").value = "";
    document.getElementById("ocode").value = " ";

    MobileLoad_show();
}



// Get Selected Group with button for 
//join to the group or leave the group
function getgrpnames(grpid) {


    document.getElementById("livesearch").innerHTML = "";

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/group/App_requests/viewrequests.php";
    

    var value = document.getElementById('id1').value;

    var vars = "id=" + value + "&gid=" + grpid;

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result3").innerHTML = return_data;
            MobileLoad_hide();


        }
    }


    hr.send(vars);


    document.getElementById('code').style.display = 'block';
    document.getElementById('codeh').style.display = 'block';
    MobileLoad_show();
}




/*
    joining to the selected group
    need to enter secret code
*/
function Addmembers(grpid) {

    document.getElementById("livesearch").innerHTML = "";
    var uid = document.getElementById('id1').value;
    var code = document.getElementById("code").value;
    
    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("result3").innerHTML = xmlhttp.responseText;
            MobileLoad_hide();
        }
    }
    xmlhttp.open("GET", "http://sep.esy.es/sep/group/App_requests/transrequests.php?id=" + uid + "&gid=" + grpid + "&code=" + code, true);
    

    MobileLoad_show();

    xmlhttp.send();

    document.getElementById('code').style.display = 'none';
    document.getElementById('codeh').style.display = 'none';
}


// Leave from the group
function deletemembers(grpid) {


    document.getElementById("livesearch").innerHTML = "";

    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/group/App_requests/transrequests.php";
    

    var uid = document.getElementById('id1').value;


    var vars = "id=" + uid + "&gid=" + grpid;


    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result3").innerHTML = return_data;
            
            MobileLoad_hide();


        }
    }


    MobileLoad_show();

    hr.send(vars);

    delgrplocal();

    document.getElementById('code').style.display = 'none';
    document.getElementById('codeh').style.display = 'none';

}


// Veiw user's joned group 
// Select Group option available from this method output
function selectgrp() {


    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/group/App_requests/viewrequests.php";
    

    var value = document.getElementById('id1').value;

    var grpid = localStorage.grpid;

    var vars = "uid=" + value + "&grpid=" + grpid;

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result2").innerHTML = return_data;
            MobileLoad_hide();


        }
    }


    hr.send(vars);

    MobileLoad_show();
}



function setuserid() {
    if (localStorage.uid == 0) {
        
        window.location = "logins.html";
    }
    else {
        document.getElementById('id1').value = localStorage.uid;
    }
    
}


// Delete selected group
function delgrplocal() {
    localStorage.removeItem("grpid");
}