﻿


//global variables
var map;
var latlntemp; // latitude longitude tempary store
var marker = null;

var markers = [];
var infowindow = [];
var contentString = [];

// Current latitude longitude
var currentlat;
var currentlng;
//

var circle;
var result;

// To display user within info window
var mecontentString = '<header class="w3-container w3-teal">' +
                                    '<h6>Me</h6><header>'



//initializing geolocation
function initGeolocation() {
    if (navigator && navigator.geolocation) {
        var watchId = navigator.geolocation.watchPosition(successCallback,
                                                          errorCallback,
                                                          { enableHighAccuracy: true, timeout: 60000, maximumAge: 0, enableHighAccuracy: true, frequency: 3000 });

    } else {
        console.log('Geolocation is not supported');
    }
}

//handling errors
function errorCallback(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.");
            break;
    }

}

// get lat & long then display map
function successCallback(position) {

    var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

    currentlat = position.coords.latitude;
    currentlng = position.coords.longitude;

    if (map == undefined) {
        var myOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.TERRAIN,
            disableDefaultUI: true,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.LEFT_TOP
            },
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_BOTTOM
            }


        }

        map = new google.maps.Map(document.getElementById("map"), myOptions);

        

        getmarker(myLatlng);
    }
    else {
        map.panTo(myLatlng);
        getmarker(myLatlng);

    }

}


//marker icon images
var image = 'images/bluedrop.png';
var imageblink = 'images/reddrop.png';


//marker creation
function getmarker(mylatlng) {

    if (marker == null) {
        marker = new google.maps.Marker({ map: map, position: mylatlng, icon: image });
    }
    else {
        marker.setPosition(mylatlng);


    }
    latlntemp = mylatlng;
}



//ajax functions


// get user details to hidden div tag
// User group details neded to group functions
function getuser() {


    var hr = new XMLHttpRequest();



    var url = "http://sep.esy.es/sep/group/App_requests/viewrequests.php";


    var value = document.getElementById('grpid').value;

    var value2 = document.getElementById('id1').value;

    var vars = "grp=" + value + "&id=" + value2 ;

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            
            document.getElementById("result").innerHTML = return_data;
            
            

        }
    }


    hr.send(vars);

    

}


/* Save current user's latitude longitude values to sync. with other
/ group members
*/
function savelatlng() {

    
    var hr = new XMLHttpRequest();


    //var url = "http://localhost/Sep/group/App_requests/transrequests.php";

    var url = "http://sep.esy.es/sep/group/App_requests/transrequests.php";

    var value = document.getElementById('id1').value;

    var lat = currentlat;
    var lng = currentlng;

    var vars = "uid=" + value + "&lat=" + lat + "&lng=" + lng;

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            
            

        }
    }


    hr.send(vars);

    

}


//varibales for intervals
var getuserinterval;
var showinmapinteval;
var savelatlnginterval;

//starting intervals for team members data sync with db
function interval() {
    
    getuserinterval = setInterval(getuser, 2000);
    showinmapinteval = setInterval(showinmap, 3000);
    savelatlnginterval = setInterval(savelatlng, 3000);
    memberschekinterval();
}

//stop running intervals
function stopinterval()
{
    clearInterval(getuserinterval);
    clearInterval(showinmapinteval);
    clearInterval(savelatlnginterval);
    stopmemberchekinterval();
    clearmarkerblink();
}


//Show markers in the map accroding to members latitude longtitude
function showinmap() {

    var max = document.getElementById('count').innerHTML;
    
    for (i = 0; i < max; i++)
    {
        var data = document.getElementById(i).innerHTML;
        var res = data.split(",");
        var pos = new google.maps.LatLng(res[0], res[1]);
        
        contentString = '<header class="w3-container w3-teal">'+
                                    '<h6>'+res[3]+' '+res[4]+'</h6><header>'
        infowindow[i] = new google.maps.InfoWindow({
            content: contentString
        });
        
        if (markers[i] == null) {
            markers[i] = new google.maps.Marker({ map: map, position: pos, icon: image });
        }
        else
        {
            markers[i].setPosition(pos);
        }


    }

    

    if (circle.getVisible()) {
        circle.setVisible(false);
    }
    setcircle();
    
   
    
}

//show team members detatils in info window(bind to the marker)
function showinfowindow() {

    

    var max = document.getElementById('count').innerHTML;

    for (i = 0; i < max; i++) {

        infowindow[i].open(map, markers[i]);

    }

    infowindowme = new google.maps.InfoWindow({
        content: mecontentString
    });

    infowindowme.open(map, marker);

}


// marker blinking function
function toggleMarker(mrkerid) {
    if (markers[mrkerid].getVisible()) {
        markers[mrkerid].setVisible(false);
    } else {
        markers[mrkerid].setVisible(true);
    }
}


var mb = []; // create array to store marker intervals


/* making intervals for markers and store in */
function markerblink(id)
{
    
    if(mb[id]==null)
    mb[id] = setInterval(function () { toggleMarker(id); }, 500);

}


//stop blinking markers and set mb[i] to null
function stopblink(id) {
    
    clearInterval(mb[id]);
    mb[id] = null;
    if (!markers[id].getVisible()) {
        markers[id].setVisible(true);
    }
}

/*clear marker blinking intervals and set markericon again*/
function clearmarkerblink()
{
    
    for (i = 0; i < mb.length; i++) {
        if(mb[i]!= null)
        {
            clearInterval(mb[i]);
            mb[i] = null;
            if (!markers[i].getVisible()) {
                markers[i].setVisible(true);
            }
            markers[i].setIcon(image);
        }

    }


}




//creating a circle with 50meter radius
function setcircle() {
 
   
        circle = new google.maps.Circle({
        center: marker.getPosition(),
        radius: 50,
        fillColor: "#E16D65",
        fillOpacity: 0.3,
        strokeOpacity: 0.0,
        strokeWeight: 0,
        map: map
    });

}





/*check for markers with the radius boundry
   if markers are away from boundry marker set to blink*/
function chekwithinradius() {

    var outmem=0;
    var max = document.getElementById('count').innerHTML;
    for (i = 0; i < max; i++) {

        result = circle.getBounds().contains(markers[i].getPosition());
        if (!result)
        {
            var data = document.getElementById(i).innerHTML;
            var res = data.split(",");
           

            markers[i].setIcon(imageblink);
            markerblink(i);
            outmem++;
        }
        else if (result) {
            markers[i].setIcon(image);
            stopblink(i);
        }
        

    }

    if (outmem == (max))
        document.getElementById("notify").innerHTML = '<header class=\"w3-container w3-red\"><h6>you are away from your team</h6></header>';
    else
        document.getElementById("notify").innerHTML = "";

    if (rMax > 50) {
        var dist = circle.getRadius();       
        document.getElementById("notify").innerHTML = "<header class=\"w3-container w3-red\"><h6>Approximatly members in " + dist + " meters away </h6></header>";
    }
    

}

//hide the circle
//incase of need to hide circle from map(not used)
function circlehide(){

    if (circle.getVisible()) {
        circle.setVisible(false);
    }
    else {
        circle.setVisible(true);
    }
}



var memchek;

//function for start to check members within radius.
function memberschekinterval()
{
    setcircle();
    memchek = setInterval(chekwithinradius, 5000);
}

//stop members checking
function stopmemberchekinterval()
{
    clearInterval(memchek);
    if (circle.getVisible()) {
        circle.setVisible(false);
    }
}


//varibale for wide scan (more radius)
var rMin = 1, rMax = 50,
    step = 1;

var rprev = 50;

var intID;

//function call to animate circle
function ant() {
   
    setAnimation();
    
}

function setAnimation() {
    var direction = 1;
    intID = setInterval(function () {
        var radius = circle.getRadius();
        if ((radius > rMax) || (radius < rMin)) {
            direction *= -1;
        }
        circle.setRadius(radius + direction * step);
    }, 50);
}


//get new scan size for wider scan
function newscan() {

    if (!isNaN(document.getElementById("Rad").value)) {

        rMax = document.getElementById("Rad").value;
        //setcircle();
        circle.setOptions({radius:rMax});
        ant();
    }
    else
    {
        alert("Please Enter Number");
    }
}

//stop wider scan
function stopnewscan()
{
    clearInterval(intID);
    circle.setOptions({ radius: rprev });
    rMax = 50;

}



//////



// set selected group id to local storage value
function setgrplocal(grpid)
{
    
    localStorage.setItem("grpid", grpid);
    selectgrp();
}

//check for group is selected or not
function checkgrpid() {
    if (localStorage.grpid == null) {
        window.location = "selectgrp.html";
    }
    else {
        document.getElementById('grpid').value = localStorage.grpid;
    }
    
}


function MobileLoad_hide() {
    $.mobile.loading("hide");
}

function MobileLoad_show() {
    $.mobile.loading("show", {
        textVisible: true
    });
}