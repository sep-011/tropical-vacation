﻿

var stringlong = null; 
var backupstring = null;
var preselect = null;

// To Show List of places 
function viewlistofplaces() {


    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/optimalnew/App_requests/viewrequests.php";

    var value = window.localStorage.getItem("setstring");

    if (value == null) {
        var vars = "all=";
    }
    else {
        var vars = "all=" + value;
        //alert("value " + vars);
    }

    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result").innerHTML = return_data;

            MobileLoad_hide();

        }
    }


    hr.send(vars);

    MobileLoad_show();



}
///////////////////

// Concatante selected locations location ids using "a"
function setLocString(locid) {
 
    if (count == 1) {
        stringlong = locid + "a";
    }
    else {
        stringlong += locid + "a";
    }    
    
    
    window.localStorage.setItem("setstring", stringlong);
    


}

// To Clear local storage an html tags
function clearhistory() {

    window.localStorage.removeItem("setstring");
    viewlistofplaces();

    document.getElementById("proceed").innerHTML = "";
    document.getElementById("proceedstatus").innerHTML = "";
    document.getElementById("headingDiv").innerHTML = "";
    document.getElementById("outputDiv").innerHTML = "";
    document.getElementById("outputdiv2").innerHTML = "";
    document.getElementById("outputdiv3").innerHTML = "";

    document.getElementById("proceedbtn").style.display = 'none';

    //arraystarts.length = 0;
    //arrayends.length = 0;
    origin1 = new google.maps.LatLng(6.9344, 79.8428);


    initialize();

    location.reload();

}



/*
    To get only the selected places
*/
function FilterLoc(lid) {

    
    var value = lid;
    var string = window.localStorage.getItem("setstring");


    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
           
            document.getElementById("proceed").innerHTML = xmlhttp.responseText;
            
            MobileLoad_hide();

            document.getElementById("proceedbtn").style.display = 'inherit';

        }
    }
    xmlhttp.open("GET", "http://sep.esy.es/sep/optimalnew/App_requests/viewrequests.php?lid=" + value + "&slid=" + string, true);

    xmlhttp.send();

    MobileLoad_show();


    $("body").animate({ scrollTop: $('#proceedstatus').offset().top - 20 }, "slow");

}




// To testing purposes
function showbackup() {

    alert("back "+backupstring+" preslect "+preselect);
    alert("set " + window.localStorage.getItem("setstring"));

}





function FilterLoctwo(lid, backstr, pre) {


    var value = lid;

    var string = backstr;

    xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

            document.getElementById("proceed").innerHTML = xmlhttp.responseText;
            MobileLoad_hide();

            document.getElementById("proceedbtn").style.display = 'inherit';

        }
    }
    xmlhttp.open("GET", "http://sep.esy.es/sep/optimalnew/App_requests/viewrequests.php?sellid=" + value + "&setlid=" + string + "&prelid=" + pre, true);

    xmlhttp.send();

    MobileLoad_show();


    $$("body").animate({ scrollTop: $('#proceedstatus').offset().top - 20 }, "slow");

}


$(document).ready(function () {
    $("#proceedbtn").click(function () {
        $("body").animate({ scrollTop: $('#headingDiv').offset().top - 20 }, "slow");
        
    });
});


// Set Travel Mode from the image icons
function setTravelMode(mode) {
    TravelModes = mode;
    document.getElementById("modename").innerHTML = "<h4>Trave Mode " + TravelModes+"</h4>";
}


// Clear The local storage and the html tags
// Geolocation init. also calling
function loadcler() {

    document.getElementById("modename").innerHTML = "<h4>Trave Mode " + TravelModes + "</h4>";

    window.localStorage.removeItem("setstring");

    document.getElementById("proceed").innerHTML = "";
    document.getElementById("proceedstatus").innerHTML = "";
    document.getElementById("headingDiv").innerHTML = "";
    document.getElementById("outputDiv").innerHTML = "";
    document.getElementById("outputdiv2").innerHTML = "";
    document.getElementById("outputdiv3").innerHTML = "";

    document.getElementById("proceedbtn").style.display = 'none';

    //arraystarts.length = 0;
    //arrayends.length = 0;


    initialize();
    initGeolocation();

}




//for direction lines draw
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();

//for directions data store;
var arraystarts = [];
var arrayends = [];
var pointscount = 0;

var TravelModes = "DRIVING";

//var flightPlanCoordinates = [];


var count = 0;
var arraylocationset = [];
var backuparray = [];

var originarray = [];
var orgincount = 0;


var map;
var geocoder;
var bounds = new google.maps.LatLngBounds();
var markersArray = [];



var origin1; // Use to store the origin latitude longitude as googlemap latlng value

var destArray = []; //Store desired places latitude longitude as googlemap latlng values


var destinationIcon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=D|FF0000|000000';
var originIcon = 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=O|FFFF00|000000';



/////////////////


//initializing geolocation
function initGeolocation() {
    if (navigator && navigator.geolocation) {
        var watchId = navigator.geolocation.watchPosition(successCallback,
                                                          errorCallback,
                                                          { enableHighAccuracy: true, timeout: 60000, maximumAge: 0, enableHighAccuracy: true, frequency: 3000 });

    } else {
        console.log('Geolocation is not supported');
    }


}

//handling errors
function errorCallback(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.");
            break;
    }

}

// get lat & long then display map
function successCallback(position) {

    origin1 = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

}

/////////////



// Map initialize
function initialize() {

    directionsDisplay = new google.maps.DirectionsRenderer();

    var opts = {
        center: new google.maps.LatLng(6.9344, 79.8428),
        zoom: 10
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), opts);
    geocoder = new google.maps.Geocoder();

    directionsDisplay.setMap(map);

}

/* Distance Calculation
    GetDistanceMatrix use callback method as a parameter
*/
function calculateDistances() {
    //alert("calculating distances ");
    backupstring = null;
    backupstring = stringlong;
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
      {
          origins: [origin1],
          destinations: destArray,
          travelMode: google.maps.TravelMode.DRIVING,
          //travelMode: google.maps.TravelMode[TravelModes],
          unitSystem: google.maps.UnitSystem.METRIC,
          avoidHighways: false,
          avoidTolls: false
      }, callback);
}


/*
    Using origin and destinations calculating the distance matrix
    distance matrix --> distance, time 
    formatted out put is displaying
*/
function callback(response, status) {
    if (status != google.maps.DistanceMatrixStatus.OK) {

        alert("You don't have any more selected places !! you can view optimal path within the map")

    } else {
        var origins = response.originAddresses;
        var destinations = response.destinationAddresses;
        var outputDiv = document.getElementById('outputDiv');
        outputDiv.innerHTML = '';


        for (var i = 0; i < origins.length; i++) {
            var results = response.rows[i].elements;
            addMarker(origins[i], false);
            for (var j = 0; j < results.length; j++) {

                addMarker(destinations[j], true);

                headingDiv.innerHTML = '<h3>Your Selected Places Distances<h3>';

                outputDiv.innerHTML += 'From ' + origins[i] + ' to ' + destinations[j]
                    + ': ' + results[j].distance.text + ' in '
                    + results[j].duration.text + '<br><br>';

                var str = backuparray[j].split(',');

                arraylocationset.push([origins[i], destinations[j], results[j].distance.value, results[j].duration.text, str[0], str[1], str[2]]);
            }
        }

        //outputdiv2.innerHTML = '<button onclick=arrange()>next</button></br>';
        outputdiv2.innerHTML = '<p><h4>To get the nearest place from the above, press the button !</h4><img onclick=arrange() src="images/arrownear.png" style="width:40px;height:40px;"></p>';
        //outputdiv2.innerHTML += '<p>Please select places that you like to travel from here..<p> </br>';

    }
}


// Map marker creation
function addMarker(location, isDestination) {
    var icon;
    if (isDestination) {
        icon = destinationIcon;
    } else {
        icon = originIcon;
    }
    geocoder.geocode({ 'address': location }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            bounds.extend(results[0].geometry.location);
            map.fitBounds(bounds);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                icon: icon
            });
            markersArray.push(marker);
        } else {
            alert('Geocode was not successful for the following reason: '
              + status);
        }
    });
}


function deleteOverlays() {
    for (var i = 0; i < markersArray.length; i++) {
        markersArray[i].setMap(null);
    }
    markersArray = [];
}





// To call initalize function
google.maps.event.addDomListener(window, 'load', initialize);






// adding destinations latitudes longitudes to array for further use
// distance that save in the array is needed to check nearest location
function Add(lat, lng, locid) {

    destArray[count] = new google.maps.LatLng(lat, lng);
    
    backuparray[count] = "" + lat + "," + lng + "," + locid + "";
    count = count + 1;

    setLocString(locid);
    viewlistofplaces();

}

// Adding destinations latitude longitudes to array
// sending the setof slected locations and previosly selected location id
// So that location details can be filter
function Addnext(lat, lng, locid) {
    destArray[count] = new google.maps.LatLng(lat, lng);
    
    backuparray[count] = "" + lat + "," + lng + "," + locid + "";
    count = count + 1;

    setLocString(locid);

    var res = backupstring.replace("" + locid + "", "0");

    backupstring = res;

    FilterLoctwo(locid, backupstring, preselect);

}


/* check for the minimum distance
/ showing the output
    set location minimum distance as origin for the next calculation
*/
function arrange() {

    
    originarray[orgincount] = origin1;



    orgincount++;
    arraylocationset.sort(function (element_a, element_b) {
        return element_a[2] - element_b[2];
    });

    
    outputdiv3.innerHTML += 'From ' + arraylocationset[0][0] + ' To <img src="images/toarrow.png" style="width:17px;height:17px;"> ' + arraylocationset[0][1]
                + ': ' + (arraylocationset[0][2]) / 1000 + 'km in '
                + arraylocationset[0][3] + '<br><br>';

    origin1 = new google.maps.LatLng(arraylocationset[0][4], arraylocationset[0][5]);


    arraystarts[orgincount - 1] = arraylocationset[0][0];
    arrayends[orgincount - 1] = arraylocationset[0][1];



    destArray.length = 0;

    count = 0;

    
    preselect = arraylocationset[0][6];


    FilterLoc(arraylocationset[0][6]);

    arraylocationset.length = 0;

}


//
// Get Latitude Longitude value
function getlalngone(lid) {


    var hr = new XMLHttpRequest();


    var url = "http://sep.esy.es/sep/optimalnew/App_requests/viewrequests.php";

    var value = lid;

    var vars = "li=" + value;


    hr.open("POST", url, true);


    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;

            MobileLoad_hide();

        }
    }


    hr.send(vars);

    MobileLoad_show();



}

//  Finding the route between locations
function calcRoute(starts, ends) {
    

    if (arrayends.length == 0) {
        alert("please select places and try again !!");
    }

    var start = starts;

    var end = ends;
    var request = {
        origin: start,
        destination: end,
        //travelMode: google.maps.TravelMode.DRIVING
        travelMode: google.maps.TravelMode[TravelModes]
    };
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
        else {
            alert("Directions service not available! please try again later")
        }
    });
}

// Going between location by showing routes
function traverseloc() {

    if (pointscount == arrayends.length - 1) {
        calcRoute(arraystarts[pointscount], arrayends[pointscount]);
        pointscount = 0
    }
    else {
        calcRoute(arraystarts[pointscount], arrayends[pointscount]);
        pointscount++;
    }

}


// To view optimal path in line
function setlines() {


    var flightPlanCoordinates = [];
    var cr;

    for (i = 0; i < originarray.length; i++) {

        flightPlanCoordinates[i] = originarray[i];
        cr = i;
    }

    flightPlanCoordinates[cr + 1] = origin1;


    var flightPath = new google.maps.Polyline({
        path: flightPlanCoordinates,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });

    flightPath.setMap(map);


}



function MobileLoad_hide() {
    $.mobile.loading("hide");
}

function MobileLoad_show() {
    $.mobile.loading("show", {
        textVisible: true
    });
}