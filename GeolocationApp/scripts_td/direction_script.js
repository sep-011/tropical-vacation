﻿var map;
var directionsDisplay;
var directionsService;
var stepDisplay;
var markerArray = [];

var latlntemp;
var marker = null;

var nlatlng;



//initializing geolocation
function initGeolocation() {
    if (navigator && navigator.geolocation) {
        var watchId = navigator.geolocation.watchPosition(successCallback,
                                                          errorCallback,
                                                          { enableHighAccuracy: true, timeout: 60000, maximumAge: 0, enableHighAccuracy: true, frequency: 3000 });

    } else {
        console.log('Geolocation is not supported');
    }

    getlocations1();
    //initialize();
}

//handling errors
function errorCallback(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.");
            break;
    }

}

// get lat & long then display map
function successCallback(position) {
    var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);    
   
    // Instantiate a directions service.
    directionsService = new google.maps.DirectionsService();

    // Create a map and center it on currentlocation.
    var currentlocation = new google.maps.LatLng(myLatlng.lat(), myLatlng.lng());
    if (map == undefined) {
        var mapOptions = {
            zoom: 13,
            center: currentlocation
        }

        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        // Create a renderer for directions and bind it to the map.
        var rendererOptions = {
            map: map
        }
        directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions)

        // to show steps text --> infowindow.
        stepDisplay = new google.maps.InfoWindow();

        
        //document.getElementById('no1').value = myLatlng.lat().toString() + "," + myLatlng.lng().toString();
        nlatlng = myLatlng.lat().toString() + "," + myLatlng.lng().toString();

        getmarker(myLatlng);

    }
    else {
        map.panTo(myLatlng);
        getmarker(myLatlng);
        document.getElementById('no1').value = myLatlng.lat().toString() + "," + myLatlng.lng().toString();
        nlatlng = myLatlng.lat().toString() + "," + myLatlng.lng().toString();

        directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions)

        // to show steps text inializing infowindow.
        stepDisplay = new google.maps.InfoWindow();
    }

}

//marker creation
function getmarker(mylatlng) {
    if (marker == null) {
        marker = new google.maps.Marker({ map: map, position: mylatlng, animation: google.maps.Animation.DROP });
    }
    else {
        marker.setPosition(mylatlng);


    }
    latlntemp = mylatlng;
}


function calcRoute() {

    // removing existing markers from the map if any.
    for (var i = 0; i < markerArray.length; i++) {
        markerArray[i].setMap(null);
    }

    //clear the array.
    markerArray = [];

    // Retrieve the start and end locations and create
    // a DirectionsRequest using WALKING/DRIVING.. directions.

    if (document.getElementById('start').selectedIndex == 0) {
        var start = nlatlng;
    }
    else {
        var start = document.getElementById('start').value;
    }
    var end = document.getElementById('end').value;
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.DRIVING
    };

    // Route the directions and pass the response to a
    // function to create markers for each step.
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            var warnings = document.getElementById('warnings_panel');
            warnings.innerHTML = '<b>' + response.routes[0].warnings + '</b>';
            directionsDisplay.setDirections(response);
            showSteps(response);
        }
    });
}

function showSteps(directionResult) {
    // For each step, place a marker, and add the text to the marker's
    // info window. Also attach the marker to an array so we
    // can keep track of it and remove it when calculating new
    // routes.
    var myRoute = directionResult.routes[0].legs[0];

    for (var i = 0; i < myRoute.steps.length; i++) {
        var marker = new google.maps.Marker({
            position: myRoute.steps[i].start_location,
            map: map
        });
        attachInstructionText(marker, myRoute.steps[i].instructions);
        markerArray[i] = marker;
    }
}

function attachInstructionText(marker, text) {
    google.maps.event.addListener(marker, 'click', function () {
        // Open an info window when the marker is clicked on,
        // containing the text of the step.
        stepDisplay.setContent(text);
        stepDisplay.open(map, marker);
    });
}




//load locations to selectbox
function getlocations1() {
    
    var hr = new XMLHttpRequest();
    
    var url = "http://sep.esy.es/sep/travel/directiondata.php";
    var query = 1;
    var vars = "all=" + query;
    hr.open("POST", url, true);
    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result").innerHTML = return_data;

           

        }
    }
    
    hr.send(vars); 
    document.getElementById("result").innerHTML = "processing...";

   

}