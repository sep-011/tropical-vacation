﻿
/* to load drop down according to the search type*/
function loadVehicle() {

    document.getElementById('details').innerHTML = "";
    document.getElementById('viewMore').innerHTML = "";

    //create http request to get values from php
    var hr = new XMLHttpRequest();

   var url = "http://sep.esy.es/sep/iteration4/minoli/Controller/viewrequests.php";
    //var url = "http://sep.esy.es/sep/iteration4/minoli/Vehicle.php";
    // var url = "http://localhost:8080/itr4/Vehicle.php";


    var index = document.getElementById('searchtype').selectedIndex;

    //check drop down option is select
    if (index != 0) {//if option is select
        
        var vars = "vhl=" + index;

        hr.open("POST", url, true);

        hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        
        hr.onreadystatechange = function () {
            if (hr.readyState == 4 && hr.status == 200) {
                var return_data = hr.responseText;
                document.getElementById("result").innerHTML = return_data;
               
                $.mobile.loading("hide");
            }
        }


        hr.send(vars);
     

        $.mobile.loading("show", {
            textVisible: true
        });


        $("body").animate({ scrollTop: $('#result').offset().top - 20 }, "slow");
    }
    else {// if option not select
        alert("Please select an option");
    }
}

/* load vehicle list */
function resultLoad(selected_type)
{
    var index = document.getElementById('result_set').value;

    //create http request to get values from php
    var hr = new XMLHttpRequest();

    var url = "http://sep.esy.es/sep/iteration4/minoli/Controller/viewrequests.php";
    //var url = "http://sep.esy.es/sep/iteration4/minoli/VehicleData.php";
    // var url = "http://localhost:8080/itr4/VehicleData.php";

   
    var vars = "ResultType=" + selected_type + "&category=" + index + "&d=";

        hr.open("POST", url, true);


        hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


        hr.onreadystatechange = function () {
            if (hr.readyState == 4 && hr.status == 200) {
                var return_data = hr.responseText;
                document.getElementById("details").innerHTML = return_data;

                $.mobile.loading("hide");
            }
        }


        hr.send(vars);       

        $.mobile.loading("show", {
            textVisible: true
        });


        $("body").animate({ scrollTop: $('#details').offset().top - 20 }, "slow");
    
}

/* view more details of the selected vehicle*/
function viewMore(id)//pass selected vehicle row id
{
    //create http request to get values from php
    var hr = new XMLHttpRequest();

    var url = "http://sep.esy.es/sep/iteration4/minoli/Controller/viewrequests.php";
    //var url = "http://sep.esy.es/sep/iteration4/minoli/MoreVehicleInfo.php";
    // var url = "http://localhost:8080/itr4/MoreVehicleInfo.php";
       

    var vars = "id=" + id;

    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("viewMore").innerHTML = return_data;

            $.mobile.loading("hide");
        }
    }


    hr.send(vars);
  

    $.mobile.loading("show", {
        textVisible: true
    });


    $("body").animate({ scrollTop: $('#viewMore').offset().top - 20 }, "slow");
}


function contactUs(id)
{
    // create http request to get values from php
    var hr = new XMLHttpRequest();

    var url = "http://sep.esy.es/sep/iteration4/minoli/Controller/viewrequests.php";

    var vars = "contact=" + id;

    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("viewMore").innerHTML = return_data;

            $.mobile.loading("hide");
        }
    }
    hr.send(vars);

    $.mobile.loading("show", {
        textVisible: true
    });

    $("body").animate({ scrollTop: $('#viewMore').offset().top - 20 }, "slow");
}