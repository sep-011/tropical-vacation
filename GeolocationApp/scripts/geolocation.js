﻿
//global variables
var map;
var latlntemp;
var marker = null;


//initializing geolocation
function initGeolocation() {
    ////samith
    //if (localStorage.lstatus!=11)
    //localStorage.uid = 0;
    if (navigator && navigator.geolocation) {
        var watchId = navigator.geolocation.watchPosition(successCallback,
                                                          errorCallback,
                                                          { enableHighAccuracy: true, timeout: 60000, maximumAge: 0, enableHighAccuracy: true, frequency: 3000 });

    } else {
        console.log('Geolocation is not supported');
    }
}

//handling errors
function errorCallback(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.");
            break;
    }

}

// get lat & long then display map
function successCallback(position) {
    var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    
    if (map == undefined) {
        var myOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.LEFT_TOP
            },
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_BOTTOM
            }


        }

        map = new google.maps.Map(document.getElementById("map"), myOptions);

        //custom element

        var centerControlDiv = document.createElement('div');
        var centerControl = new CenterControl(centerControlDiv, map);

        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP].push(centerControlDiv);
        //

        getmarker(myLatlng);
    }
    else {
        map.panTo(myLatlng);
        getmarker(myLatlng);

    }
    
}

//marker creation
function getmarker(mylatlng) {
    if (marker == null) {
        marker = new google.maps.Marker({ map: map, position: mylatlng, animation: google.maps.Animation.DROP });
    }
    else {
        marker.setPosition(mylatlng);


    }
    latlntemp = mylatlng;
}


$(document).on("swipeleft swiperight", "#pageone", function (e) {
    console.log('swiped!!')
    //check if there is no open panel on the page because otherwise
    // We do this by checking the data that the framework stores on the page element (panel: open).
    if ($.mobile.activePage.jqmData("panel") !== "open" && $(e.target).closest(".bxslider").length === 0) {
        if (e.type === "swiperight") {
            $("#myPanel").panel("open");
        }
        // else if (e.type === "swipeleft") {
        //    $("#myPanel").panel("open");
        //}
    }
    else if ($.mobile.activePage.jqmData("panel") == "open" && $(e.target).closest(".bxslider").length === 0) {
        $("#myPanel").panel("close");
        //$("#myPanel").panel("close");
    }
});



//center screen function

function CenterControl(controlDiv, map) {

    // Set CSS for the control border
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginBottom = '22px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to relocate you !';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior
    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = 'Center';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to
    // latlntemp -> current location 
    google.maps.event.addDomListener(controlUI, 'click', function () {
        map.setCenter(latlntemp)
    });

    
}



