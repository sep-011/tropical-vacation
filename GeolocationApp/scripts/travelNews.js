﻿
/* load travel news according to the drop down*/
function loadTravelNews() {

    //create http request to get values from php
    var hr = new XMLHttpRequest();

    var url = "http://sep.esy.es/sep/iteration4/minoli/Controller/viewrequests.php";
  //  var url = "http://sep.esy.es/sep/iteration4/minoli/newsFeed.php";
   // var url = "http://localhost:8080/itr4/newsFeed.php";

    //get selected index from dropdown lists
    var index = document.getElementById('travelType').selectedIndex;

    if (index != 0) {//if dropdown option selected

        var query = document.getElementById('travelType').value;       

        var vars = "type=" + query;

        hr.open("POST", url, true);


        hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


        hr.onreadystatechange = function () {
            if (hr.readyState == 4 && hr.status == 200) {
                var return_data = hr.responseText;
                document.getElementById("result").innerHTML = return_data;

                $.mobile.loading("hide");
            }
        }
        
        hr.send(vars);      

        $.mobile.loading("show", {
            textVisible: true
        });


        $("body").animate({ scrollTop: $('#result').offset().top - 20 }, "slow");
    }
    else//if dropdown option not selected
    {
        alert("Please select and option");
    }
    

}

function goUp()
{
    $("body").animate({ scrollTop: $('#result').offset().top - 20 }, "slow");
}