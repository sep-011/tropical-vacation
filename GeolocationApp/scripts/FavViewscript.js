﻿
//adding favlocation to database
//function called in viewAllFav.php (post --> all)table loading..
//viewall() function called -->then go to viewAllFav.php
function Addfavloc(id, uid) {
    
    var hr = new XMLHttpRequest();
    
    var url = "http://sep.esy.es/sep/addfav.php";

    var query = id;
    var usid = uid;
    var vars = "sel=" + query + "&ud=" + usid;
    hr.open("POST", url, true);
    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("status").innerHTML = return_data;

            $.mobile.loading("hide");

        }
    }
    
    hr.send(vars); 

    //document.getElementById("status").innerHTML = "processing...";

    $.mobile.loading("show", {
        textVisible: true
    });


    $("body").animate({ scrollTop: $('#status').offset().top - 20 }, "slow");

    viewall();

}



//to show selected fav location details
//function called in viewAllFav.php (post --> fav)table loading..
//viewfavlist() function called -->then go to viewAllFav.php
function getfavdetails(placeid) {
    
    var hr = new XMLHttpRequest();
    
    var url = "http://sep.esy.es/sep/showfavlocDetails.php";
    var query = placeid;
    var vars = "q=" + query;
    hr.open("POST", url, true);
    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("status").innerHTML = return_data;

            $.mobile.loading("hide");

        }
    }
    
    hr.send(vars); 
    //document.getElementById("status").innerHTML = "processing...";

    $.mobile.loading("show", {
        textVisible: true
    });

    $("body").animate({ scrollTop: $('#status').offset().top - 20 }, "slow");
}


//remove favlocation from list
function removefavloc(id, uid) {
    
    var hr = new XMLHttpRequest();
    
    var url = "http://sep.esy.es/sep/removefav.php";

    var query = id;
    var usid = uid;
    var vars = "locid=" + query + "&ud=" + usid;
    hr.open("POST", url, true);
    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("status").innerHTML = return_data;

            $.mobile.loading("hide");


        }
    }
    
    hr.send(vars); 

    //document.getElementById("status").innerHTML = "processing...";

    $.mobile.loading("show", {
        textVisible: true
    });


    $("body").animate({ scrollTop: $('#status').offset().top - 20 }, "slow");

    viewfavlist();

}




//view all the location stored in database
function viewall() {
    
    var hr = new XMLHttpRequest();
    
    var url = "http://sep.esy.es/sep/viewAllFav.php";
    var query = document.getElementById("id1").value;
    var vars = "all=" + query;
    hr.open("POST", url, true);
    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result").innerHTML = return_data;

            $.mobile.loading("hide");

        }
    }
    
    hr.send(vars); 
    //document.getElementById("result").innerHTML = "processing...";

    $.mobile.loading("show", {
        textVisible: true
    });

    //$("body").animate({ scrollTop: 120 }, "slow");
    $("body").animate({ scrollTop: $('#result').offset().top - 20 }, "slow");
}


//view fav location according to user id
function viewfavlist() {
    
    var hr = new XMLHttpRequest();
    
    var url = "http://sep.esy.es/sep/viewAllFav.php";
    var query = document.getElementById("id1").value;
    var vars = "fav=" + query;
    hr.open("POST", url, true);
    
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    
    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            var return_data = hr.responseText;
            document.getElementById("result").innerHTML = return_data;

            $.mobile.loading("hide");

        }
    }
    
    hr.send(vars); 
    //document.getElementById("result").innerHTML = "processing...";

    $.mobile.loading("show", {
        textVisible: true
    });
    
    $("body").animate({ scrollTop: $('#status').offset().top - 20 }, "slow");
}


function setuserid() {
    if (localStorage.uid == 0) {
        //alert('please log to comment');
        window.location = "logins.html";
    }
    else {
        document.getElementById('id1').value = localStorage.uid;
    }
    //document.getElementById('id1').value = localStorage.uid;
}



