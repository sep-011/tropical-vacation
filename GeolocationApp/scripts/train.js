﻿//define global variables

var map;

var stationName = [];
var nearplaces = [];

var marker = null;
var place_list = 0,position_list=0;
var nlatlng;
var infowindow;
var latitude;
var longitude;



/* to view train schedule  */
function viewSchedule() {

    //get selected index from dropdown lists
    var strat_id = document.getElementById('start').selectedIndex;
    var end_id = document.getElementById('end').selectedIndex;
   
    // check if the selected index is zero
    if (strat_id != 0 && end_id != 0) {
        
        //create http request to get values from php
        var hr = new XMLHttpRequest();

        var url = "http://sep.esy.es/sep/iteration4/minoli/Controller/viewrequests.php";
       // var url = "http://sep.esy.es/sep/iteration4/minoli/trainSchedule.php";
        // var url = "http://localhost:8080/itr4/trainSchedule.php";

        //get selected value from dropdown list
        var start = document.getElementById("start").value;
        var end = document.getElementById("end").value;

        //send selected dropdowns values to php
        var vars = "start=" + start + "&end=" + end;
        hr.open("POST", url, true);

        hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        hr.onreadystatechange = function () {
            if (hr.readyState == 4 && hr.status == 200) {
                //get returned data from php
                var return_data = hr.responseText;
                //store returned data
                document.getElementById("train_result").innerHTML = return_data;
               
                $.mobile.loading("hide");
            }
        }

        hr.send(vars);

        $.mobile.loading("show", {
            textVisible: true
        });


        $("body").animate({ scrollTop: $('#train_result').offset().top - 20 }, "slow");
    }
    else  // if the selected index is zero
    {
        alert("Please select starting and ending locations!!");
    }

   
  
    
}

/* view more information about the selected train */
function viewMoreTrain(id) { // pass selected row id

    //create http request to get values from php
    var hr = new XMLHttpRequest();

    var url = "http://sep.esy.es/sep/iteration4/minoli/Controller/viewrequests.php";
     //var url = "http://sep.esy.es/sep/iteration4/minoli/Viewmore.php";
    //var url = "http://localhost:8080/itr4/Viewmore.php";

    //send selected row id to php
    var vars = "view=" + id;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {
            //get returned data from php
            var return_data = hr.responseText;

            document.getElementById("train_more_details").innerHTML = return_data;
           
            $.mobile.loading("hide");
        }
    }

    hr.send(vars);

    $.mobile.loading("show", {
        textVisible: true
    });


    $("body").animate({ scrollTop: $('#train_more_details').offset().top - 20 }, "slow");

}


/*initializing geolocation*/
function initGeolocation(tid) {//pass selected train id

    //display map

    //track current location
    var myLatlng = new google.maps.LatLng(6.9336, 79.8508);

    var currentlocation = new google.maps.LatLng(myLatlng.lat(), myLatlng.lng());

    if (map == undefined) {
        var mapOptions = {
            zoom: 12,
            center: currentlocation
        }
        //load the map
        map = new google.maps.Map(document.getElementById('canvas'), mapOptions);
        nlatlng = myLatlng.lat().toString() + "," + myLatlng.lng().toString();
    }
        //if success go to the viewmap
        viewMap(tid);
    
   
  
}





/*marker creation*/
function getmarker(mylatlng) {

    if (marker == null) {
        marker = new google.maps.Marker({ map: map, position: mylatlng, animation: google.maps.Animation.DROP });
    }
    else {
        marker.setPosition(mylatlng);

    }
   
}

/*view stations on the map*/
function viewMap(tid) {//pass train id

    //create a http request
    var hr = new XMLHttpRequest();

    var url = "http://sep.esy.es/sep/iteration4/minoli/Controller/viewrequests.php";
   // var url = "http://sep.esy.es/sep/iteration4/minoli/ViewTrainMap.php";
   // var url = "http://localhost:8080/itr4//ViewTrainMap.php";


    var vars = "viewMap=" + tid;
    hr.open("POST", url, true);

    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    hr.onreadystatechange = function () {
        if (hr.readyState == 4 && hr.status == 200) {

            //get reurn values from php to cordova app
            var return_data = hr.responseText;

            document.getElementById("train_map").innerHTML = return_data;
           
            $.mobile.loading("hide");

            // get lenght of the station list
            var listLen = document.getElementById("locLen").value;

            for (var i = 0; i < listLen; i++) {
                var str1 = "id";
                var str2 = i;
                var station = str1.concat(str2);

                var no = document.getElementById(station).value;
                stationName[i] = no;
                
            }
            position_list = stationName.length;

            // display stations on the map
            var category = document.getElementById("cat").value;
            if (category==1)
            {
                var start = document.getElementById("len").value;
                trainRoute(start);                
                place_list = 0;
            }
            else if (category == 2)
            {
                var start = document.getElementById("len").value;
                var start2 = document.getElementById("len2").value;
                trainRoute(start);
                place_list = place_list - 2;
                trainRoute(start2);                
                place_list = 0;
            }
            else if (category == 3)
            {
                var start = document.getElementById("len").value;
                var start2 = document.getElementById("len2").value;
                var start3 = document.getElementById("len3").value;
                trainRoute(start);
                place_list = place_list - 2;
                trainRoute(start2);
                place_list = place_list - 2;
                trainRoute(start3);
                place_list = 0;
            }
                       
            
           //get nearest location list
            var listL = document.getElementById("locNear").value;

            for (var i = 0; i < listL; i++) {
                var str1 = "idloc";
                var str2 = i;
                var location = str1.concat(str2);

                var no = document.getElementById(location).value;
                nearplaces[i] = no;
               
            }
            position_list = nearplaces.length;
            //alert(g);
           var nearLoc = document.getElementById("locP").value;
          // alert(nearLoc);
            OtherRoute(nearLoc);
            
        }
    }

    hr.send(vars);

    $.mobile.loading("show", {
        textVisible: true
    });


    $("body").animate({ scrollTop: $('#canvas').offset().top - 20 }, "slow");
   
}


/* mark train root */
function trainRoute(s) {
   
    // break down the coordinate string to longitude and latitude
    var coordinateString = s;
    var coordinateString_arr = coordinateString.split("/");
    var flightPlanCoordinates = [];

    for (var key in coordinateString_arr) {
        var temp = coordinateString_arr[key].split(",");
        flightPlanCoordinates.push(new google.maps.LatLng(parseFloat(temp[0]), parseFloat(temp[1])));
       

        latitude = parseFloat(temp[0]);
        longitude = parseFloat(temp[1]);

        // mark stations on map
        stations(stationName[place_list]);//pass station name

        place_list++;
      
       
    } 

    // drop poly line between two stations
    var flightPath = new google.maps.Polyline({
        path: flightPlanCoordinates,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });

    flightPath.setMap(map);
    
}


/* mark stations on the map */
function stations(ltext) {
   
    var trainicon = new google.maps.MarkerImage(" http://sep.esy.es/sep/iteration4/minoli/images/blueicon.png",
    new google.maps.Size(32, 32), new google.maps.Point(0, 0),
    new google.maps.Point(16, 32));

    // set info window
    infowindow = new google.maps.InfoWindow();

    var addplace = new google.maps.LatLng(latitude, longitude);

    if (map == undefined) {
        map = new google.maps.Map(document.getElementById("canvas"), {
            zoom: 13,
            center: nlatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
    }
    else { }

    marker = null;
    marker = new google.maps.Marker({
        position: addplace,
        map: map,
        icon: trainicon

    });
   
    //show information
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(ltext);
        infowindow.open(map, this);
    });

   
}

/* to check drop downs arre select */
function getSelectedid()
{
   
    var strat_id = document.getElementById('start').selectedIndex;
    var end_id = document.getElementById('end').selectedIndex;
    
    if (strat_id != 0 && end_id != 0) {
        initGeolocation(strat_id);
    }
    else
    {
        alert("Please select starting and ending locations!!");
    }
    

}

/* get near by places to stations on the map*/
function OtherRoute(s) {

    // break down the coordinate string to longitude and latitude
    var coordinateString = s;
    var coordinateString_arr = coordinateString.split("/");
    var flightPlanCoordinates = [];

    for (var key in coordinateString_arr) {
        var temp = coordinateString_arr[key].split(",");
        flightPlanCoordinates.push(new google.maps.LatLng(parseFloat(temp[0]), parseFloat(temp[1])));
        
        latitude = parseFloat(temp[0]);
        longitude = parseFloat(temp[1]);

        // mark places on the map
        otrPlaces(nearplaces[place_list]);//pass place name
        place_list++;

       
    }

   

}

/* mark near by places to stations on the map*/
function otrPlaces(ltext) {
    //set icon
    var icon = new google.maps.MarkerImage("http://sep.esy.es/sep/iteration4/minoli/images/green.png",
    new google.maps.Size(32, 32), new google.maps.Point(0, 0),
    new google.maps.Point(16, 32));
    //set info window
    infowindow = new google.maps.InfoWindow();

    var addplace = new google.maps.LatLng(latitude, longitude);

    if (map == undefined) {
        map = new google.maps.Map(document.getElementById("canvas"), {
            zoom: 13,
            center: nlatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
    }
    else { }

    marker = null;
    marker = new google.maps.Marker({
        position: addplace,
        map: map,
        icon: icon

    });
    
    //show information
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(ltext);
        infowindow.open(map, this);
    });




}