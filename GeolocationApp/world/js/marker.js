﻿//javascript prototype object
//poiData : single label information
function Marker(poiData) {

    this.poiData = poiData;

    this.selected = false;

    //AR.GeoLocation represents the location of the label
    var markerLocation = new AR.GeoLocation(poiData.latitude, poiData.longitude, poiData.altitude);

    //Create a ImageDrawable with ImageResource. ImageDrawable are displayed in the AR view
    //passed arguments are a ImageResource, height and options
    this.markerDrawable_idle = new AR.ImageDrawable(World.markerImage_idle, 2.5, {
        zOrder: 0,
        opacity: 1.0,

        //onClick function called when user tapped on a Label icon
        //pass the clicked label as arguments
        onClick: Marker.prototype.markerClicked(this)        
    });

    this.markerDrawable_selected = new AR.ImageDrawable(World.markerImage_selected, 2.5, {
        zOrder: 0,
        opacity: 0.0,
        onClick: null,
    });

    //AR.Label is used to display some text in AR view
    //passed arguments are a text to display, height and options
    this.titleLabel = new AR.Label(poiData.title.trunc(10), 1, {
        zOrder: 1,
        offsetY: 0.55,
        style: {
            textColor: '#FFFFFF',
            fontStyle: AR.CONST.FONT_STYLE.BOLD
        }
    });

    this.descriptionLabel = new AR.Label(poiData.description.trunc(15), 0.8, {
        zOrder: 1,
        offsetY: -0.55,
        style: {
            textColor: '#FFFFFF'
        }
    });

    //Create a circle (dot) to represent the location on radar
    //passed arguments are a radius and options
    this.radarDot = new AR.Circle(0.03, {
        horizontalAnchor: AR.CONST.HORIZONTAL_ANCHOR.CENTER,
        opacity: 0.8,
        style: {
            fillColor: "#ffffff"
        }
    });

    //insert AR.Circle object to a array
    this.radarDotArray = [];
    this.radarDotArray.push(this.radarDot);

    //To display neccessary  drawables on a specific location in the camera view 
    //and to display the label location on radar
    //passed arguments are location(AR.GeoLocation object), drawables
    this.makerObject = new AR.GeoObject(markerLocation, {
        drawables: {
            cam: [this.markerDrawable_idle, this.markerDrawable_selected, this.titleLabel, this.descriptionLabel],
            radar: this.radarDotArray
        }
    });

    return this;
}

/**
*To display part of the label for lengthy text
*@param {number} n : maximum length
*/
String.prototype.trunc = function (n) {
    return this.substr(0, n - 1) + (this.length > n ? '...' : '');
};

/**
*To display selected label and default label when user tapped on a label
*@param {AR.ImageDrawable object} marker : label drawable
*/
Marker.prototype.markerClicked = function (marker) {
    return function () {
        if (marker.selected == false) {
            Marker.prototype.selectMarker(marker);
            //marker.markerDrawable_idle.opacity = 0.0;
            //marker.markerDrawable_selected.opacity = 1.0;
            //marker.selected = true;

            //var dist = marker.makerObject.locations[0].distanceToUser();
            //var distanceVal = (dist > 999) ? ((dist / 1000).toFixed(2) + " km") : (Math.round(dist) + " m");

            //$("#locTitle").html(marker.poiData.title);
            //$("#locDescription").html(marker.poiData.description);
            //$("#locDistance").html(distanceVal);

            //$("#panelpop").slideToggle("slow");
        }
        else {
            Marker.prototype.removeMarker(marker);
            //marker.markerDrawable_idle.opacity = 1.0;
            //marker.markerDrawable_selected.opacity = 0.0;
            //marker.selected = false;
            //$("#panelpop").slideToggle("slow");
        }
    };
};

Marker.prototype.selectMarker = function (marker) {

    //To remove any other label selections
    if (World.selectedMarker != null) {
        Marker.prototype.removeMarker(World.selectedMarker);
    };

    World.selectedMarker = marker;

    marker.markerDrawable_idle.opacity = 0.0;
    if (marker.poiData.labelImg == "default") {
        marker.markerDrawable_selected.opacity = 1.0;
    }
    else {
        marker.markerDrawable_selected.opacity = 0.5;
    }
    marker.selected = true;

    var dist = marker.makerObject.locations[0].distanceToUser();
    var distanceVal = World.getKmAndMeterValue(dist);//(dist > 999) ? ((dist / 1000).toFixed(2) + " km") : (Math.round(dist) + " m");
    World.locId = marker.poiData.id;

    $("#locTitle").html(marker.poiData.title);
    $("#locDescription").html(marker.poiData.description);
    $("#locDistance").html(distanceVal);

    //open the bottom panel
    $("#panelpop").slideToggle("slow");
};

Marker.prototype.removeMarker = function (marker) {
    marker.markerDrawable_idle.opacity = 1.0;
    marker.markerDrawable_selected.opacity = 0.0;
    marker.selected = false;
    $("#panelpop").slideToggle("slow");
    World.selectedMarker = null;
}
