﻿//javascript object
var World = {

    locId: 0,

    //default range in meters
    initRange: 20,

    initRangeSet: false,

    selectedMarker: null,

    initiallyLoadedData: false,

    //To store all the AR labels as "Marker" objects
    markers: [],
   
    markerImage_idle: null,
    markerImage_selected: null,

    //This function called when gps signal tracked
    locationChanged: function (lat, lon, alt, acc) {

        //load data from server
        if (!World.initiallyLoadedData) {
            World.loadData();
            World.initiallyLoadedData = true;

            //set the visible distance to default visible disance
            AR.context.scene.cullingDistance = World.initRange;
        }
    },

    //Load location label data from the server
    loadData: function () {

        //enable and display the radar view
        World.displayRadar();

        //request location label data (server generate details as json data)
        $.getJSON("http://sep.esy.es/admin/admin_labels/jsongen.php", function (jsonData) {

            var data = jsonData;
            World.markers = [];

            //loop through all the label details and create an "Marker" object for each label
            for (var i = 0; i < data.length; i++) {
                var singlePoi = {
                    "id": data[i].id,
                    "latitude": parseFloat(data[i].latitude),
                    "longitude": parseFloat(data[i].longitude),
                    "altitude": data[i].altitude == "default" ? AR.CONST.UNKNOWN_ALTITUDE : parseFloat(data[i].altitude),
                    "title": data[i].name,
                    "description": data[i].description,
                    "labelImg": data[i].labelImg
                };


                var imgUrl = "";
                var imgUrlSelect = "";
                
                if (singlePoi.labelImg == "default") {
                    //Default label images
                    imgUrl = "http://sep.esy.es/sep/images/ar/marker_idle.png";
                    imgUrlSelect = "http://sep.esy.es/sep/images/ar/marker_selected.png";
                }
                else {
                    imgUrl = "http://sep.esy.es/" + singlePoi.labelImg;
                    imgUrlSelect = imgUrl;
                }

                //create a ImageResources that can be used in ImageDrawables to display in AR view
                World.markerImage_idle = new AR.ImageResource(imgUrl);
                World.markerImage_selected = new AR.ImageResource(imgUrlSelect);

                //fill the "markers" array 
                World.markers.push(new Marker(singlePoi));
            }
        
        }).error(function (err) {
            alert("Turn on the internet connection and restart");
        });
    },

    //This function called when select range option in settings
    rangeSet: function () {
        if (World.markers.length != 0) {
            var range;
            World.markers.sort(function (a, b) { return a.makerObject.locations[0].distanceToUser() - b.makerObject.locations[0].distanceToUser() });
            var maxDist = World.markers[World.markers.length - 1].makerObject.locations[0].distanceToUser();
            maxDist += (15 / 100) * maxDist;

            //set initial slider value
            if (World.initRangeSet == false) {
                range = World.initRange;
                var setInitSlider = (range / maxDist) * 100;
                setInitSlider = setInitSlider > 100 ? 100 : setInitSlider;
                //$("#maxRange").html(range);

                World.initRangeSet = true;

                $('#page3').page();

                $("#rangeSlider").val(setInitSlider);
                $("#rangeSlider").slider("refresh");

                //add slider change event
                $("#rangeSlider").change(function () {
                    World.rangeSet();
                    //$("#maxRange").append("ch ");
                });

            }
            else {
                var sliderVal = $("#rangeSlider").val();
                range = (sliderVal / 100) * maxDist;
                //$("#maxRange").html(range);

                //set the visible distance
                //value should be a positive whole number
                AR.context.scene.cullingDistance = Math.max(Math.ceil(range), 1);
            }
            sliderVal = $("#rangeSlider").val();
            $("#maxRange").html(World.getKmAndMeterValue(Math.ceil(range)) + " (" + sliderVal + "%)");

            //get the visible label count
            var count = 0;
            for (var i = 0; i < World.markers.length; i++) {
                if (World.markers[i].makerObject.locations[0].distanceToUser() > range) {
                    break;
                }
                count++;
            }
            $("#noLocations").html(count);
            var near = count == 0 ? "0 Locations" : World.getKmAndMeterValue(World.markers[0].makerObject.locations[0].distanceToUser());
            var far = count == 0 ? "0 Locations" : World.getKmAndMeterValue(World.markers[count - 1].makerObject.locations[0].distanceToUser());
            $("#nearPl").html(near);
            $("#farPl").html(far);
            //World.markers.sort(function (a, b) { return a.makerObject.locations[0].distanceToUser() - b.makerObject.locations[0].distanceToUser() });
            //$("#noLocations").append(World.markers[0].makerObject.locations[0].distanceToUser() + " , ");
            //$("#noLocations").append(World.markers[1].makerObject.locations[0].distanceToUser() + " , ");
            //$("#noLocations").append(World.markers[2].makerObject.locations[0].distanceToUser() + " , ");
            //var maxDist = World.markers[World.markers.length - 1].makerObject.locations[0].distanceToUser();
            //$("#noLocations").append(maxDist + " , ");
            //var setInitSlider = (range / maxDist) * 100;
            //setInitSlider = setInitSlider > 100 ? 100 : setInitSlider;
            //$("#noLocations").html(sliderVal + " , ");
            //$("#rangeSlider").val(setInitSlider);
            //$("#rangeSlider").slider("refresh");


        }
        else {
            alert("no loc");
        }
    },

    getKmAndMeterValue: function (dist) {
        return (dist > 999) ? ((dist / 1000).toFixed(2) + " km") : (Math.round(dist) + " m");
    },

    //setSelected: function (marker) {
    //    marker.markerDrawable_idle.opacity = 0.0;
    //},

    //setDeSelected: function (marker) {
    //    marker.markerDrawable_idle.opacity = 0.0;
    //},

    //Display the radar view
    displayRadar: function () {
        AR.radar.container = document.getElementById("radarDiv");
        AR.radar.background = new AR.ImageResource("assets/radar_bg.png");
        AR.radar.centerX = 0.5;
        AR.radar.centerY = 0.5;

        //radius with regards to the width of the background image
        AR.radar.radius = 0.3; 
        AR.radar.enabled = true;
    },
};

AR.context.onLocationChanged = World.locationChanged;