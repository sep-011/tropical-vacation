var locJsonData = [{
	"id": "1",
	"longitude": "79.861534",
	"latitude": "6.910709",
	"description": "Colombo",
	"altitude": "100.0",
	"name": "National Museum"
}, {
	"id": "2",
	"longitude": "79.872810",
	"latitude": "6.901669",
	"description": "Bandaranaike Memorial International Conference Hall",
	"altitude": "100.0",
	"name": "BMICH"
}, {
	"id": "3",
	"longitude": "79.861711",
	"latitude": "6.899821",
	"description": "University of Colombo",
	"altitude": "100.0",
	"name": "Faculty of law"
}, {
    "id": "3",
    "longitude": "79.854791",
    "latitude": "6.905959",
    "description": "description goes here.................",
    "altitude": "100.0",
    "name": "Mahanama College"
},{
	"id": "4",
    "longitude": "79.974310",
    "latitude": "6.914991",
    "description": "Sri Lanka Institute of Information Technology",
    "altitude": "100.0",
    "name": "SLIIT"
}
];