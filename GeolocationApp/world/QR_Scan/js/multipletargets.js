var World = {
    loaded: false,

	init: function initFn() {
		this.createOverlays();
	},

	createOverlays: function createOverlaysFn() {		

	    //Create a AR.ClientTracker object with a target collection
	    this.tracker = new AR.ClientTracker("assets/tracker.wtc", {
			onLoaded: this.worldLoaded  //
	    });	    

	    //create a ImageResource that can be used in ImageDrawable 
		var imgAukana = new AR.ImageResource("assets/Aukana_Buddha_Statue.png");

	    //Create a ImageDrawable with above ImageResource. ImageDrawable are displayed in the AR view
	    //passed arguments are a ImageResource, height and position
		var overlayAukana = new AR.ImageDrawable(imgAukana, 1.5, {
		    offsetX: 0.12,
		    offsetY: -0.01,
		    onClick: function () {
		        getPanelDetails("2");
		    }
		});


	    //Create a AR.Trackable2DObject to track a specific target in the target collection(tracker) and 
	    //to display the neccessary  drawables in the camera view
	    //passed arguments are tracker object, target name specified as in the 
		//tracker(target collection), drawables
		var aukanaMain = new AR.Trackable2DObject(this.tracker, "aukanaMain", {
		    drawables: {
		        //drawable that should be added to the camera view
		        cam: overlayAukana
		    },
		});


        /*Weather*/

	    //Create a HtmlDrawable for weather widget
	    //uri is provided from accueWeather
	    //passed arguments : uri, width (height adjest to aspect ratio) and options
		var weatherWidget = new AR.HtmlDrawable({
		    uri: "assets/weather.html"
		}, 1.5, {
		    viewportHeight: 124, //height of the html drawable loading container
		    backgroundColor: "#FFFFFF",
		    offsetX: 0.75,
		    offsetY: 0.2,
		    horizontalAnchor: AR.CONST.HORIZONTAL_ANCHOR.RIGHT,
		    verticalAnchor: AR.CONST.VERTICAL_ANCHOR.TOP,
		    clickThroughEnabled: true,
		    allowDocumentLocationChanges: false, //To stop changed location, load inside html drawable whrn click
		    onDocumentLocationChanged: function onDocumentLocationChangedFn(uri) {
		        AR.context.openInBrowser(uri);
		    }
		});

	    //create a ImageResource to display with weather widget
		var aukanaWeatherImg = new AR.ImageResource("assets/Aukana_weather.png");

	    //Create a ImageDrawable with above ImageResource. ImageDrawable are displayed in the AR view
	    //passed arguments are a ImageResource, height and position
	    var overlayAukanaWeather = new AR.ImageDrawable(aukanaWeatherImg, 0.5, {
	        offsetY: 0.5
	    });

        //Same as before. Used two drawables to display
	    var aukanaMain = new AR.Trackable2DObject(this.tracker, "aukanaWeather", {
	        //drawables that should be added to the camera view
	        drawables: {
	            cam: [overlayAukanaWeather, weatherWidget]
	        },
	    });


	    /*Video*/

	    // Create play button(drawable) which is used for starting the video
	    var playButtonImg = new AR.ImageResource("assets/playButton.png");

	    //Create a ImageDrawable with above ImageResource.
        //this drawable is  position to display on top of video
	    //passed arguments are a ImageResource, height and options
		var playButton = new AR.ImageDrawable(playButtonImg, 0.3, {
		    enabled: false,

            //flag to indicate button click
		    clicked: false, 
		    onClick: function playButtonClicked() {
		        video.play(1);

		        //flag to indicate video playing or not
		        video.playing = true;
		        playButton.clicked = true;
		    },
		    offsetY: -0.3
		});


	    //Create a video darawable that can be display in the AR view
	    //passed parameters : video uri, height (width is adjusted), options

	    //var video = new AR.VideoDrawable("http://sep.esy.es/sep/scan_qr/vid/video.mp4", 0.50, {

		var video = new AR.VideoDrawable("assets/video.mp4", 0.50, {
		    offsetY: playButton.offsetY,

            //display the playButton icon after video is loaded
		    onLoaded: function videoLoaded() {
		        playButton.enabled = true;
		    },

            //Hide the play button and enable(allow to appear on screen) the video just brfore the video start
		    onPlaybackStarted: function beforeStart() {
		        playButton.enabled = false;
		        video.enabled = true;
		    },
            //After playing finish
		    onFinishedPlaying: function afterFinished() {
		        playButton.enabled = true;
		        video.playing = false;
		        video.enabled = false;
		    },
            //video click event
		    onClick: function videoStartPause() {
		        if (playButton.clicked) {
		            playButton.clicked = false;
		        } else if (video.playing) {
		            video.pause();
		            video.playing = false;
		        } else {
		            video.resume();
		            video.playing = true;
		        }
		    }
		});


		var videoAukana = new AR.Trackable2DObject(this.tracker, "aukanaVideo", {
            //drawables that should be added to the camera view
		    drawables: {
		        cam: [video, playButton]
		    },

            //when target image visible to the camera
		    onEnterFieldOfVision: function onEnterFieldOfVisionFn() {
		        if (video.playing) {
		            video.resume();
		        }
		    },

		    //trigger when the target image visibility has changed from visible to invisible.
		    onExitFieldOfVision: function onExitFieldOfVisionFn() {
		        if (video.playing) {
		            video.pause();
		        }
		    }
		});
	},

	worldLoaded: function worldLoadedFn() {
        World.loaded=true
	}
};

//Start the image scanning
World.init();