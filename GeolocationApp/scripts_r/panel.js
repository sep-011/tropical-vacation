﻿//$(document).on("swipeleft swiperight", "#pageone", function (e) {
//    console.log('swiped!!')
//    //check if there is no open panel on the page because otherwise
//    // We do this by checking the data that the framework stores on the page element (panel: open).
//    if ($.mobile.activePage.jqmData("panel") !== "open" && $(e.target).closest(".bxslider").length === 0) {
//        if (e.type === "swiperight") {
//            $("#myPanel").panel("open");
//        }
//        // else if (e.type === "swipeleft") {
//        //    $("#myPanel").panel("open");
//        //}
//    }
//    else if ($.mobile.activePage.jqmData("panel") == "open" && $(e.target).closest(".bxslider").length === 0) {
//        $("#myPanel").panel("close");
//        //$("#myPanel").panel("close");
//    }
//});

$(document).on("pagecreate", "#pageone", function () {
    $(document).on("swipeleft swiperight", "#pageone", function (e) {
        // We check if there is no open panel on the page because otherwise
        // a swipe to close the left panel would also open the right panel (and v.v.).
        // We do this by checking the data that the framework stores on the page element (panel: open).
        if ($(".ui-page-active").jqmData("panel") !== "open") {
            if (e.type === "swipeleft") {
                $("#right-panel").panel("open");
            } else if (e.type === "swiperight") {
                $("#myPanel").panel("open");
            }
        }
    });
});

$(document).on("pagecreate", "#pagetwo", function () {
    $(document).on("swipeleft swiperight", "#pagetwo", function (e) {
        // We check if there is no open panel on the page because otherwise
        // a swipe to close the left panel would also open the right panel (and v.v.).
        // We do this by checking the data that the framework stores on the page element (panel: open).
        if ($(".ui-page-active").jqmData("panel") !== "open") {
            if (e.type === "swipeleft") {
                $("#right-panel").panel("open");
            } else if (e.type === "swiperight") {
                $("#myPanel").panel("open");
            }
        }
    });
});

$(function () {
    $("#myPanel").panel();
})