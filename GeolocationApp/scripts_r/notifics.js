﻿/**
*Get new notification count and display in the notification count bubble
*/
function getNotificationCount() {
    
    $(".noti_bubble").hide();

    //If user not logged in exit the function
    if (localStorage.uid == 0 || localStorage.uid == undefined) {
        return;
    }

    var notificCount = $.ajax({
        url: "http://sep.esy.es/sep/img_tst/testimg.php",
        method: "get",
        data: { notific: uid },
        dataType: "html"
    });

    //When ajax call success
    notificCount.done(function (msg) {
        //if there are new notifications, show the notification bubble and insert the notification count
        if (msg > 0) {
            $(".noti_bubble").html(msg).show();
        }
    });

    //When ajax call failed
    notificCount.fail(function (jqXHR, textStatus) {
        //alert("notifications request failed: " + textStatus);
    });
}

/*Get the new notification count after page load*/
$(function () {
    getNotificationCount();
})

/**
*To get details of a specific NEW notification
*@param {number} notific_Id : Id of requesting notification
*/
function getNotificDetails(notific_Id) {
    
    document.getElementById("notifications").innerHTML = "<DIV style=\"text-align:center\"><img src=\"images/images_r/728.GIF\" style=\"text-align:center\"></DIV>";
    var notific = $.ajax({
        url: "http://sep.esy.es/sep/img_tst/testimg.php",
        method: "get",
        data: { getNotific: notific_Id, uid: uid },
        dataType: "html"
    });

    //When ajax call success
    notific.done(function (msg) {

        getNotificationCount(); //To reduce the bubble notification count after opening a new notification

        var details = msg;
        var locAvail = false;
        var indexVal = 0;

        //To check whether the location also shared with this post
        if (msg.substring(0, 3) == "***") {
            indexVal = msg.lastIndexOf("***", 30);
            if (indexVal != -1) {
                var locAvail = true;
                details = msg.substring(indexVal + 3);
            }
        }

        document.getElementById("loadNotific").innerHTML = "";
        document.getElementById("notifications").innerHTML = details;

        //If location also shared with this post, call the 
        //load map method to display the location on a map
        if (locAvail) {
            var coord = msg.substring(3, indexVal).split("/");
            loadMap(coord[0], coord[1]);
        }

        //To create the image gallery using received images
        $(document).ready(function () {
            $('.thumbs a').touchTouch();
        });
    });

    //When ajax call failed
    notific.fail(function (jqXHR, textStatus) {
        document.getElementById("loadNotific").innerHTML = "";
        document.getElementById("notifications").innerHTML = "";
        alert("Request failed: " + textStatus);
    });
}

//Use this vartiable to decide what should happen with the hide/allow button
var action="";

/**
*To get details of a own post 
*@param {number} notific_Id : Id of requesting notification
*/
function getOwnNotificDetails(notific_Id) {

    document.getElementById("notifications").innerHTML = "<DIV style=\"text-align:center\"><img src=\"images/images_r/728.GIF\" style=\"text-align:center\"></DIV>";
    var notific = $.ajax({
        url: "http://sep.esy.es/sep/img_tst/testimg.php",
        method: "get",
        data: { getOwnNotific: notific_Id, uid: uid },
        dataType: "html"
    });

    //When ajax call success
    notific.done(function (msg) {
        var details = msg;
        var locAvail = false;
        var indexVal = 0;

        //To check whether the location also shared with this post
        if (msg.substring(0, 3) == "***") {
            indexVal = msg.lastIndexOf("***", 30);
            if (indexVal != -1) {
                var locAvail = true;
                details = msg.substring(indexVal + 3);
            }
        }
        
        document.getElementById("notifications").innerHTML = details;

        //Check whether this post is hidden
        var hidden = $("#actions").data("ishidden");
        if (hidden == 'Y') {
            document.getElementById("loadNotific").innerHTML = "<p style=\"color:red;\">This post is hidden</p>";
            document.getElementById("hiddenBtn").innerHTML = "Allow";
            action = "allow";
        }
        else {
            document.getElementById("loadNotific").innerHTML = "";
            action = "hide";
        }

        //If location also shared with this post, call the 
        //load map method to display the location on a map
        if (locAvail) {
            var coord = msg.substring(3, indexVal).split("/");
            loadMap(coord[0], coord[1]);
        }

        //To create the image gallery using received images
        $(document).ready(function () {
            $('.thumbs a').touchTouch();
        });
    });

    //When ajax call failed
    notific.fail(function (jqXHR, textStatus) {
        document.getElementById("loadNotific").innerHTML = "";
        document.getElementById("notifications").innerHTML = "";
        alert("Request failed: " + textStatus);
    });
}

/**
*To display location in a map 
*@param {number} lat : latitude of the location
*@param {number} lon : longitude of the location
*/
function loadMap(lat, lon) {

    //make the center of the map to the arguments coordinates
    var mapCenter = new google.maps.LatLng(lat, lon);

    var mapProp = {
        center: mapCenter,
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

    //Create location marker
    var marker = new google.maps.Marker({
        position: mapCenter,
    });

    //Add marker to the map
    marker.setMap(map);
}

var shareId;

/**
*To open the confirmation dialog box for delete operation of a post
*/
function confirmDeleteShare() {

    //share id from data attribute
    shareId = $("#actions").data("shareid");

    // to prevent opening of popupDialog on outside click
    var openConfirm = true; 
    $("#popupMenu").popup("close");

    //Open the confirmation dialog box after the first dialog box closed
    $("#popupMenu").on("popupafterclose", function () {
        if (openConfirm) {
            $("#popupDialog").popup("open");
            openConfirm = false;
        }
    });
}

/**
*To delete a shared post
*This function called when click on the delete cofirmation button
*/
function deleteShare() {
    document.getElementById("notifications").innerHTML = "";
    document.getElementById("loadNotific").innerHTML = "<img src=\"images/images_r/728.GIF\" style=\"text-align:center\">";

    var deleteNotific = $.ajax({
        url: "http://sep.esy.es/sep/img_tst/testimg.php",
        method: "get",
        data: { deleteNotific: shareId },
        dataType: "html"
    });

    //When ajax call success
    deleteNotific.done(function (msg) {
        document.getElementById("loadNotific").innerHTML = "";
        deleteSuccess();
    });

    //When ajax call failed
    deleteNotific.fail(function (jqXHR, textStatus) {
        document.getElementById("loadNotific").innerHTML = "";
        document.getElementById("notifications").innerHTML = "";
        alert("Request failed: " + textStatus);
    });

    /*Go to the previous page after successfully deleted the post*/
    function deleteSuccess() {
        document.getElementById("notifications").innerHTML = "<BR><DIV style=\"text-align:center\"><B>Successfully Deleted...<B><DIV>";
        setTimeout(function () {
            location.reload();
        }, 2500);
    }
}

/**
*To hide a shared post from others
*This function called when click on the hide button
*/
function hideShare() {

    //get the share id from data attribute
    shareId = $("#actions").data("shareid");

    document.getElementById("loadNotific").innerHTML = "<img src=\"images/images_r/728.GIF\" style=\"text-align:center\">";

    var hideNotific = $.ajax({
        url: "http://sep.esy.es/sep/img_tst/testimg.php",
        method: "get",
        data: { hideNotific: shareId, action: action },
        dataType: "html"
    });

    //When ajax call success
    hideNotific.done(function (msg) {
        if (action == "hide") {
            document.getElementById("loadNotific").innerHTML = "<p style=\"color:red;\">This post is hidden</p>";
            document.getElementById("hiddenBtn").innerHTML = "Allow";
            action = "allow";
        }
        else {
            document.getElementById("loadNotific").innerHTML = "";
            document.getElementById("hiddenBtn").innerHTML = "Hide";
            action = "hide";
        }
    });

    //When ajax call failed
    hideNotific.fail(function (jqXHR, textStatus) {
        document.getElementById("loadNotific").innerHTML = "";
        alert("Request failed: " + textStatus);
    });
}