﻿var app = {
    requiredFeatures: ["geo"],
    arExperienceUrl: "www/world/index.html",
    isDeviceSupported: false,

    startupConfiguration:
    {
        "camera_position": "back"
    },

    initialize: function () {
        this.bindEvents();
    },

    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    onDeviceReady: function () {
        app.wikitudePlugin = cordova.require("com.wikitude.phonegap.WikitudePlugin.WikitudePlugin");
        app.wikitudePlugin.isDeviceSupported(app.onDeviceSupported, app.onError, app.requiredFeatures);
    },

    onDeviceSupported: function () {
        app.wikitudePlugin.setOnUrlInvokeCallback(app.onUrlInvoke);
        app.wikitudePlugin.loadARchitectWorld(
            app.onARExperienceLoadedSuccessful,
            app.onError,
            app.arExperienceUrl,
            app.requiredFeatures,
            app.startupConfiguration
        );
    },

    onARExperienceLoadedSuccessful: function () {

    },

    onError: function (errorMsg) {
        alert(errorMsg);
    },

    onUrlInvoke: function () {

    }

};
